#include "stdafx.h"
#include "SingleTagElementParsingStrategy.h"
#include "StringUtils.h"
#include "HTMLUtils.h"

using namespace HTMLParserLibrary;

void SingleTagElementParsingStrategy::Parse(const std::string& content, size_t& pos, Element& element)
{
    LOG_INFO << "Begin";

    if (ParseIfDoctype(content, pos, element))
    {
        LOG_INFO << "Single-tag element " << element.GetName() << " was parsed.";
        LOG_VERBOSE << "End";
        return;
    }

    std::vector<std::string> closeTagElements = { "/area", "/base", "/basefont" , "/bgsound" , "/br", "/col" , "/command" , "/embed" , "/hr" , "/img" , "/input" , "/isindex" , "/keygen" , "/link" , "/meta" , "/param" , "/source" , "/track" , "/wbr" , "/path" };
    std::vector<std::string> selfClosedTagElements = { "area/", "base/", "basefont/" , "bgsound/" , "br/", "col/" , "command/" , "embed/" , "hr/" , "img/" , "input/" , "isindex/" , "keygen/" , "link/" , "meta/" , "param/" , "source/" , "track/" , "wbr/" , "path/" };
    std::vector<std::string> selfClosedWhitespaceTagElements = { "area /", "base /", "basefont /" , "bgsound /" , "br /", "col /" , "command /" , "embed /" , "hr /" , "img /" , "input /" , "isindex /" , "keygen /" , "link /" , "meta /" , "param /" , "source /" , "track /" , "wbr /" , "path /" };

    size_t greaterThanSignPos = content.find(ParserConstants::GREATER_THAN_SIGN, pos);

    if (greaterThanSignPos == std::string::npos)
    {
        LOG_FATAL << "Incorrect element. It doesn't have '>' symbol";
        throw HTMLParsingException("Incorrect element. It doesn't have '>' symbol");
    }

    std::string tagContent = content.substr(pos + 1, greaterThanSignPos - pos - 1);
    std::string tagName = "";
    std::string attributesString = "";

    size_t whiteSpacePos = tagContent.find(ParserConstants::WHITE_SPACE);

    if (whiteSpacePos != std::string::npos)
    {
        size_t slashPos = tagContent.find(ParserConstants::SLASH, pos);
        if (slashPos == std::string::npos || whiteSpacePos < slashPos)
        {
            attributesString = tagContent.substr(whiteSpacePos + 1);
            attributesString.erase(attributesString.find_last_not_of(ParserConstants::SLASH) + 1);
            attributesString.erase(attributesString.find_last_not_of(ParserConstants::WHITE_SPACE) + 1);

            tagContent.erase(whiteSpacePos, attributesString.size() + 1);
        }
        else
        {
            LOG_FATAL << "Incorrect single-tag element.";
            throw HTMLParsingException("Incorrect single-tag element.");
        }
    }

    if (std::find(ParserConstants::SINGLE_TAG_ELEMENTS.begin(), ParserConstants::SINGLE_TAG_ELEMENTS.end(), tagContent) != ParserConstants::SINGLE_TAG_ELEMENTS.end())
    {
        tagName = tagContent;
    }

    if (std::find(closeTagElements.begin(), closeTagElements.end(), tagContent) != closeTagElements.end() ||
        std::find(selfClosedTagElements.begin(), selfClosedTagElements.end(), tagContent) != selfClosedTagElements.end())
    {
        tagContent.erase(std::remove(tagContent.begin(), tagContent.end(), ParserConstants::SLASH), tagContent.end());
        tagName = tagContent;
    }

    if (std::find(selfClosedWhitespaceTagElements.begin(), selfClosedWhitespaceTagElements.end(), tagContent) != selfClosedWhitespaceTagElements.end())
    {
        tagContent.erase(std::remove(tagContent.begin(), tagContent.end(), ParserConstants::SLASH), tagContent.end());
        tagContent.erase(std::remove(tagContent.begin(), tagContent.end(), ParserConstants::WHITE_SPACE), tagContent.end());
        tagName = tagContent;
    }

    if (!tagName.empty())
    {
        element.SetName(tagName);
        pos = greaterThanSignPos + 1;

        LOG_INFO << "Single-tag element " << element.GetName() << " was parsed.";

        std::vector<Attribute> attributes;
        if (!attributesString.empty())
        {
            ParseAttributes(attributesString, attributes);
            element.AddAttributes(attributes);
        }
    }

    LOG_VERBOSE << "End";
}

bool SingleTagElementParsingStrategy::ParseIfDoctype(const std::string& content, size_t& pos, Element& element) const
{
    const std::string DOCTYPE = "!DOCTYPE";

    if (content.find(DOCTYPE, pos) == 1)
    {
        pos = content.find(ParserConstants::GREATER_THAN_SIGN, pos) + 1;
        element.SetName(DOCTYPE);

        return true;
    }

    return false;
}