#pragma once

#include "Element.h"

namespace HTMLParserLibrary
{
    class DOMBuilder
    {
    public:
        void InsertElement(const Element& element, size_t nesting);

        const std::vector<Element>& GetDOM() const;

    private:
        std::vector<Element>& GetPlaceToInsert(std::vector<Element>& elements, size_t nesting) const;

    private:
        std::vector<Element> elements_;
    };
}