#include "stdafx.h"
#include "DOMParser.h"
#include "StringUtils.h"
#include "ElementParser.h"
#include "ParsingStrategyFactory.h"
#include "HTMLUtils.h"

using namespace HTMLParserLibrary;

void DOMParser::ParseElements(const std::string& documentContent, std::vector<Element>& elements)
{
    if (documentContent.empty())
    {
        LOG_INFO << "Document is empty.";
        return;
    }

    std::string content(documentContent);

    RemoveUnnecessaryElements(content);

    if (!IsStringContainsHTML(content))
    {
        LOG_FATAL << "Document doesn't contain HTML elements. Script and style elements library doesn't support.";
        throw HTMLParsingException("Document doesn't contain HTML elements. Script and style elements library doesn't support.");
    }

    size_t pos = 0;
    size_t size = content.size();
    size_t nesting = 0;
    std::stack<std::string> tagNames;

    LOG_VERBOSE << "Start parsing document.";

    while (pos < size - 1)
    {
        size_t oldPos = pos;
        pos = content.find(ParserConstants::LESSER_THAN_SIGN, pos);

        ParsingStrategyFactory parsingStrategyFactory;
        std::unique_ptr<ElementParser> elementParser;

        Element element;

        if (oldPos + 1 < pos)                                        //Element value
        {
            elementParser.reset(new ElementParser(parsingStrategyFactory.GetStrategy(ParsingType::ValueElement)));
            elementParser->Parse(content, oldPos, element);

            domBuilder_.InsertElement(element, ++nesting);
            --nesting;

            LOG_INFO << "Value element " << element.GetValue() << " was parsed.";
            continue;
        }

        elementParser.reset(new ElementParser(parsingStrategyFactory.GetStrategy(ParsingType::SingleTagElement))); //Single-tag element
        elementParser->Parse(content, pos, element);

        if (!element.GetName().empty())
        {
            domBuilder_.InsertElement(element, ++nesting);
            --nesting;
            continue;
        }

        if (content[pos + 1] == ParserConstants::SLASH)              //Close tag
        {
            if (tagNames.empty())
            {
                LOG_FATAL << "There is close tag before open.";
                throw HTMLParsingException("There is close tag before open.");
            }

            auto greaterThanSignPos = content.find(ParserConstants::GREATER_THAN_SIGN, pos);
            std::string name = content.substr(pos + 2, greaterThanSignPos - pos - 2);

            if (name != tagNames.top())
            {
                LOG_FATAL << "There are different open and close tags.";
                throw HTMLParsingException("There are different open and close tags.");
            }

            LOG_VERBOSE << "Close tag of default element " << tagNames.top() << "  was parsed.";
            tagNames.pop();

            pos = greaterThanSignPos + 1;
            --nesting;

            continue;
        }

        elementParser.reset(new ElementParser(parsingStrategyFactory.GetStrategy(ParsingType::DefaultElement))); //Default element
        elementParser->Parse(content, pos, element);

        tagNames.push(element.GetName());
        domBuilder_.InsertElement(element, ++nesting);
        LOG_INFO << "Open tag of default element " << element.GetName() << " was parsed.";
    }

    if (nesting)
    {
        LOG_FATAL << "Incorrect document. Different number of open and close tags.";
        throw HTMLParsingException("Incorrect document. Different number of open and close tags.");
    }

    auto dom = domBuilder_.GetDOM();
    elements.assign(dom.begin(), dom.end());
}

void DOMParser::RemoveUnnecessaryElements(std::string& content) const
{
    RemoveStyleElements(content);
    RemoveScriptElements(content);
    RemoveComments(content);

    Utils::ReplaceAllEntities(content, ParserConstants::NEW_LINE_SYMBOL, "");
    Utils::ReplaceAllEntities(content, ParserConstants::TABULATION_SYMBOL, "");
}