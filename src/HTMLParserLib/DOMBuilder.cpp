#include "stdafx.h"
#include "DOMBuilder.h"

using namespace HTMLParserLibrary;

void DOMBuilder::InsertElement(const Element& element, size_t nesting)
{
    GetPlaceToInsert(elements_, nesting).push_back(element);
}

const std::vector<Element>& DOMBuilder::GetDOM() const
{
    return elements_;
}

std::vector<Element>& DOMBuilder::GetPlaceToInsert(std::vector<Element>& elements, size_t nesting) const
{
    while (nesting > 1)
    {
        if (elements.empty())
        {
            LOG_FATAL << "Incorrect place in DOM to insert.";
            throw HTMLParsingException("Incorrect place in DOM to insert.");
        }

        return GetPlaceToInsert(elements.back().GetChildren(), --nesting);
    }

    return elements;
}