#include "stdafx.h"
#include "Attribute.h"

using namespace HTMLParserLibrary;

Attribute::Attribute(const std::string& name)
    : Attribute(name, "")
{ }

Attribute::Attribute(const std::string& name, const std::string& value)
    : name_(name), value_(value)
{ }

const std::string& Attribute::GetName() const
{
    return name_;
}

const std::string& Attribute::GetValue() const
{
    return value_;
}

void Attribute::SetName(const std::string& name)
{
    name_ = name;
}

void Attribute::SetValue(const std::string& value)
{
    value_ = value;
}

void Attribute::SetItem(const std::string& name, const std::string& value)
{
    name_ = name;
    value_ = value;
}