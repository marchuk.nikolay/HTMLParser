#include "stdafx.h"
#include "ElementParser.h"

using namespace HTMLParserLibrary;

ElementParser::ElementParser(ParsingStrategyPtr parsingStrategy) :
    parsingStrategy_(parsingStrategy)
{
}

void ElementParser::Parse(const std::string& content, size_t& pos, Element& element)
{
    parsingStrategy_->Parse(content, pos, element);
}