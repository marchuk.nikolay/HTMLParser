#include "stdafx.h"
#include "Element.h"
#include "StringUtils.h"
#include "HTMLUtils.h"
#include "DOMSaver.h"
#include "DOMParser.h"

using namespace HTMLParserLibrary;

Element::Element()
    : Element("")
{ }

Element::Element(const std::string& name)
    : name_(name)
{ }

const std::string& Element::GetName() const
{
    return name_;
}

const std::string& Element::GetValue() const
{
    return value_;
}

void Element::SetName(const std::string& name)
{
    name_ = name;
}

void Element::SetValue(const std::string& value)
{
    value_ = value;
}

std::vector<Attribute>& Element::GetAttributes()
{
    return attributes_;
}

void Element::AddAttribute(const Attribute& attribute)
{
    attributes_.push_back(attribute);
}

void Element::AddAttributes(const std::vector<Attribute>& attributes)
{
    attributes_.insert(attributes_.end(), attributes.begin(), attributes.end());
}

void Element::RemoveAttribute(const std::string& attributeName)
{
    auto iter = std::find_if(attributes_.begin(), attributes_.end(), [&attributeName](const Attribute& attribute)
    {
        return attribute.GetName() == attributeName;
    });

    if (iter == attributes_.end())
    {
        LOG_ERROR << "Attribute for removing doesn't exist.";
        throw HTMLParsingException("Attribute for removing doesn't exist.");
    }

    attributes_.erase(std::remove_if(attributes_.begin(), attributes_.end(), [&attributeName](const Attribute& attribute)
    {
        return attribute.GetName() == attributeName;
    }), attributes_.end());
}

void Element::RemoveAttributes()
{
    attributes_.clear();
}

std::vector<Element>& Element::GetChildren()
{
    return children_;
}

void Element::AddToBegin(const Element& element)
{
    children_.insert(children_.begin(), element);
}

void Element::AddToEnd(const Element& element)
{
    children_.push_back(element);
}

void Element::AddChildren(const std::vector<Element>& children)
{
    children_.insert(children_.end(), children.begin(), children.end());
}

void Element::Search(const std::string& selector, std::vector<Element>& result)
{
    if (selector.empty())
    {
        return;
    }

    std::vector<std::string> selectors;
    Utils::SplitString(selector, ' ', selectors);

    RecursiveSearch(selectors.at(0), result);
    selectors.erase(selectors.begin());

    std::vector<Element> endResult;

    for (const auto& selectorItem : selectors)
    {
        for (auto& element : result)
        {
            for (auto& child : element.GetChildren())
            {
                if (child.IsElementSatisfyingSelector(selectorItem))
                {
                    endResult.push_back(child);
                }
            }
        }

        result.assign(endResult.begin(), endResult.end());
        endResult.clear();
    }
}

void Element::RecursiveSearch(const std::string& selector, std::vector<Element>& result)
{
    for (auto& child : children_)
    {
        if (child.IsElementSatisfyingSelector(selector))
        {
            result.push_back(child);
        }

        if (!child.GetChildren().empty())
        {
            std::vector<Element> childResult;
            child.RecursiveSearch(selector, childResult);

            result.insert(result.end(), childResult.begin(), childResult.end());
        }
    }
}

bool Element::HasAttribute(const std::string& attributeName, const std::string& attributeValue)
{
    auto iter = std::find_if(attributes_.begin(), attributes_.end(), [&attributeName, &attributeValue](const Attribute& attribute)
    {
        return attribute.GetName() == attributeName && attribute.GetValue() == attributeValue;
    });

    if (iter == attributes_.end())
    {
        return false;
    }

    return true;
}

bool Element::IsElementSatisfyingSelector(const std::string& selector)
{
    if (name_ == selector)                                     //Check tag selector
    {
        return true;
    }

    if (selector.at(0) == '.')                                 //Check that element has class attribute
    {
        if (HasAttribute("class", &selector.at(1)))
        {
            return true;
        }
    }

    if (selector.at(0) == '#')                                 //Check that element has id attribute
    {
        if (HasAttribute("id", &selector.at(1)))
        {
            return true;
        }
    }

    if (selector.at(0) == '[')                                 //Check that element has any other attribute
    {
        std::string attributeString(selector.begin() + 1, selector.end() - 1);

        auto equalSignPos = attributeString.find('=');
        if (equalSignPos != std::string::npos)
        {
            std::string attributeName = attributeString.substr(0, equalSignPos);
            std::string attributeValue = attributeString.substr(equalSignPos + 1);

            if (HasAttribute(attributeName, attributeValue))
            {
                return true;
            }
        }

        if (HasAttribute(attributeString, ""))
        {
            return true;
        }
    }

    return false;
}

void Element::GetHTML(std::string& html)
{
    DOMSaver saver;

    Element root;
    root.AddToEnd(*this);

    saver.Save(root, html);
}

void Element::SetHTML(const std::string& html)
{
    DOMParser parser;
    std::vector<Element> elements;
    parser.ParseElements(html, elements);

    children_.assign(elements.begin(), elements.end());
}

void Element::GetText(std::string& text)
{
    GetHTML(text);

    HTMLParserLibrary::RemoveHTMLElements(text);
}

void Element::SetText(const std::string& text)
{
    children_.clear();

    if (text.empty())
    {
        return;
    }

    Element child;
    child.SetValue(text);

    AddToEnd(child);
}