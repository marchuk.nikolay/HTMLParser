#include "stdafx.h"
#include "ValueElementParsingStrategy.h"

using namespace HTMLParserLibrary;

void ValueElementParsingStrategy::Parse(const std::string& content, size_t& pos, Element& element)
{
    LOG_INFO << "Begin";

    auto oldPos = pos;
    pos = content.find(ParserConstants::LESSER_THAN_SIGN, pos);

    if (pos == std::string::npos)
    {
        LOG_FATAL << "Incorrect element. It doesn't have close tag.";
        throw HTMLParsingException("Incorrect element. It doesn't have close tag.");
    }

    std::string value = content.substr(oldPos, pos - oldPos);
    element.SetValue(value);

    LOG_VERBOSE << "End";
}