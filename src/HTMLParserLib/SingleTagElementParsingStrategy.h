#pragma once

#include "IParsingStrategy.h"

namespace HTMLParserLibrary
{
    class SingleTagElementParsingStrategy : public IParsingStrategy
    {
    public:
        virtual void Parse(const std::string& content, size_t& pos, Element& element);

    private:
        bool ParseIfDoctype(const std::string& content, size_t& pos, Element& element) const;
    };
}