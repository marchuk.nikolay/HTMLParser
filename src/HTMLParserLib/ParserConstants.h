#pragma once

#include "stdafx.h"

namespace HTMLParserLibrary
{
    class ParserConstants
    {
    public:
        static const char LESSER_THAN_SIGN;
        static const char GREATER_THAN_SIGN;
        static const char SLASH;
        static const char EQUAL_SIGN;
        static const char WHITE_SPACE;
        static const char QUOTE;

        static const std::string STYLE_OPEN_TAG_BEGINNING;
        static const std::string STYLE_CLOSE_TAG;
        static const std::string SCRIPT_OPEN_TAG_BEGINNING;
        static const std::string SCRIPT_CLOSE_TAG;
        static const std::string COMMENT_OPEN_TAG;
        static const std::string COMMENT_CLOSE_TAG;

        static const std::string NEW_LINE_SYMBOL;
        static const std::string TABULATION_SYMBOL;

        static const std::vector<std::string> SINGLE_TAG_ELEMENTS;
    };
}