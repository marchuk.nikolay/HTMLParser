#pragma once

#include "IParsingStrategy.h"

namespace HTMLParserLibrary
{
    class ValueElementParsingStrategy : public IParsingStrategy
    {
    public:
        virtual void Parse(const std::string& content, size_t& pos, Element& element);
    };
}