#pragma once

#include "Element.h"

namespace HTMLParserLibrary
{
    class IParsingStrategy
    {
    public:
        virtual void Parse(const std::string& content, size_t& pos, Element& element) = 0;
        virtual ~IParsingStrategy() {}
    };

    using ParsingStrategyPtr = std::shared_ptr<IParsingStrategy>;
}