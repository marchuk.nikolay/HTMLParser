#include "stdafx.h"
#include "HTMLUtils.h"
#include "StringUtils.h"

void HTMLParserLibrary::RemoveStyleElements(std::string& str)
{
    RemoveElements(str, ParserConstants::STYLE_OPEN_TAG_BEGINNING, ParserConstants::STYLE_CLOSE_TAG);
}

void HTMLParserLibrary::RemoveScriptElements(std::string& str)
{
    RemoveElements(str, ParserConstants::SCRIPT_OPEN_TAG_BEGINNING, ParserConstants::SCRIPT_CLOSE_TAG);
}

void HTMLParserLibrary::RemoveComments(std::string& str)
{
    RemoveElements(str, ParserConstants::COMMENT_OPEN_TAG, ParserConstants::COMMENT_CLOSE_TAG);
}

void HTMLParserLibrary::RemoveElements(std::string& str, const std::string& openTag, const std::string& closeTag)
{
    size_t pos = 0;
    size_t size = str.size();

    while (pos < size - 1)
    {
        pos = str.find(openTag, pos);
        size_t closeTagPos;

        if (pos != std::string::npos)
        {
            closeTagPos = str.find(closeTag, pos);
        }
        else
        {
            closeTagPos = str.find(closeTag, 0);
        }

        if ((pos == std::string::npos) && (closeTagPos == std::string::npos))
        {
            break;
        }

        if ((pos == std::string::npos) && (closeTagPos != std::string::npos))
        {
            std::string exceptionMessage("Invalid document. It has " + closeTag + " tag, but doesn't have " + openTag);
            LOG_FATAL << exceptionMessage;
            throw HTMLParsingException(exceptionMessage.c_str());
        }

        if ((pos != std::string::npos) && (closeTagPos == std::string::npos))
        {
            std::string exceptionMessage("Invalid document. It has " + openTag + " tag, but doesn't have " + closeTag);
            LOG_FATAL << exceptionMessage;
            throw HTMLParsingException(exceptionMessage.c_str());
        }

        std::string findStr = str.substr(pos, closeTagPos - pos + closeTag.size());
        Utils::ReplaceAllEntities(str, findStr, "");
    }
}

void HTMLParserLibrary::RemoveHTMLElements(std::string& str)
{
    str = std::regex_replace(str, std::regex("<(/?[^>]+)>"), "");

    if ((str.find('<') != std::string::npos) || (str.find('>') != std::string::npos))
    {
        throw HTMLParsingException("Invalid HTML");
    }
}

bool HTMLParserLibrary::IsStringContainsHTML(const std::string& str)
{
    return std::regex_search(str, std::regex("<(/?[^>]+)>"));
}

void HTMLParserLibrary::ParseAttributes(const std::string& content, std::vector<Attribute>& attributes)
{
    size_t pos = 0;
    size_t size = content.size();

    while (pos < size - 1)
    {
        auto whiteSpacePos = content.find(ParserConstants::WHITE_SPACE, pos);
        auto equalSignPos = content.find(ParserConstants::EQUAL_SIGN, pos);

        if (whiteSpacePos == std::string::npos && equalSignPos == std::string::npos)
        {
            Attribute attribute(content.substr(pos, content.size() - pos));

            LOG_INFO << attribute.GetName() << " attribute was parsed.";

            attributes.push_back(std::move(attribute));

            return;
        }

        if (equalSignPos == std::string::npos || whiteSpacePos < equalSignPos)
        {
            Attribute attribute(content.substr(pos, whiteSpacePos - pos));

            pos = whiteSpacePos + 1;
            LOG_INFO << attribute.GetName() << " attribute was parsed.";

            attributes.push_back(std::move(attribute));

            continue;
        }
        else
        {
            std::string attributeName = content.substr(pos, equalSignPos - pos);

            auto attributeValueStartPos = equalSignPos + 2;
            auto attributeValueEndPos = content.find(ParserConstants::QUOTE, attributeValueStartPos);

            std::string attributeValue = content.substr(attributeValueStartPos, attributeValueEndPos - attributeValueStartPos);

            Attribute attribute(attributeName, attributeValue);

            pos = attributeValueEndPos + 2;
            LOG_INFO << attribute.GetName() << " attribute was parsed.";

            attributes.push_back(std::move(attribute));
        }
    }
}