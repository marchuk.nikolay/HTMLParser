#pragma once

#include "stdafx.h"

namespace HTMLParserLibrary
{
    class Attribute
    {
    public:
        explicit Attribute(const std::string& name);
        Attribute(const std::string& name, const std::string& value);

        const std::string& GetName() const;
        const std::string& GetValue() const;

        void SetName(const std::string& name);
        void SetValue(const std::string& value);
        void SetItem(const std::string& name, const std::string& value);

    private:
        std::string name_;
        std::string value_;
    };
}