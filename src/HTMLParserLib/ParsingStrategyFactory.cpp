#include "stdafx.h"
#include "ParsingStrategyFactory.h"
#include "ValueElementParsingStrategy.h"
#include "SingleTagElementParsingStrategy.h"
#include "DefaultElementParsingStrategy.h"

using namespace HTMLParserLibrary;

ParsingStrategyPtr ParsingStrategyFactory::GetStrategy(const ParsingType& parsingType)
{
    switch (parsingType)
    {
    case ParsingType::ValueElement:
        return ParsingStrategyPtr(new ValueElementParsingStrategy);
    case ParsingType::SingleTagElement:
        return ParsingStrategyPtr(new SingleTagElementParsingStrategy);
    case ParsingType::DefaultElement:
        return ParsingStrategyPtr(new DefaultElementParsingStrategy);
    default:
        LOG_FATAL << "Unknown parsing strategy.";
        throw HTMLParsingException("Unknown parsing strategy.");
    }
}