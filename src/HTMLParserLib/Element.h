#pragma once

#include "stdafx.h"
#include "Attribute.h"

namespace HTMLParserLibrary
{
    class Element
    {
    public:
        Element();
        explicit Element(const std::string& name);

        const std::string& GetName() const;
        const std::string& GetValue() const;

        void SetName(const std::string& name);
        void SetValue(const std::string& value);

        std::vector<Attribute>& GetAttributes();
        void AddAttribute(const Attribute& attribute);
        void AddAttributes(const std::vector<Attribute>& attributes);
        void RemoveAttribute(const std::string& attributeName);
        void RemoveAttributes();

        std::vector<Element>& GetChildren();
        void AddChildren(const std::vector<Element>& children);
        void AddToBegin(const Element& element);
        void AddToEnd(const Element& element);

        void Search(const std::string& selector, std::vector<Element>& result);
        bool HasAttribute(const std::string& attributeName, const std::string& attributeValue);
        bool IsElementSatisfyingSelector(const std::string& selector);

        void GetHTML(std::string& html);
        void SetHTML(const std::string& html);

        void GetText(std::string& text);
        void SetText(const std::string& text);

    private:
        void RecursiveSearch(const std::string& selector, std::vector<Element>& result);

    private:
        std::string name_;
        std::string value_;
        std::vector<Attribute> attributes_;
        std::vector<Element> children_;
    };
}