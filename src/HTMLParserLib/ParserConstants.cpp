#include "stdafx.h"
#include "ParserConstants.h"

using namespace HTMLParserLibrary;

const char ParserConstants::LESSER_THAN_SIGN = '<';
const char ParserConstants::GREATER_THAN_SIGN = '>';
const char ParserConstants::SLASH = '/';
const char ParserConstants::EQUAL_SIGN = '=';
const char ParserConstants::WHITE_SPACE = ' ';
const char ParserConstants::QUOTE = '\"';

const std::string ParserConstants::STYLE_OPEN_TAG_BEGINNING = "<style";
const std::string ParserConstants::STYLE_CLOSE_TAG = "</style>";
const std::string ParserConstants::SCRIPT_OPEN_TAG_BEGINNING = "<script";
const std::string ParserConstants::SCRIPT_CLOSE_TAG = "</script>";
const std::string ParserConstants::COMMENT_OPEN_TAG = "<!--";
const std::string ParserConstants::COMMENT_CLOSE_TAG = "-->";

const std::string ParserConstants::NEW_LINE_SYMBOL = "\n";
const std::string ParserConstants::TABULATION_SYMBOL = "\t";

const std::vector<std::string> ParserConstants::SINGLE_TAG_ELEMENTS = { "area", "base", "basefont" , "bgsound" , "br", "col" , "command" , "embed" , "hr" , "img" , "input" , "isindex" , "keygen" , "link" , "meta" , "param" , "source" , "track" , "wbr", "path", "!DOCTYPE" };