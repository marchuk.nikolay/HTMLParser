#pragma once

#include "Element.h"
#include "Attribute.h"

namespace HTMLParserLibrary
{
    class TagCreator
    {
    public:
        void CreateOpenTag(Element& element, std::string& tag) const;
        void CreateCloseTag(const Element& element, std::string& tag) const;

    private:
        void SaveAttributesToString(const std::vector<Attribute>& attributes, std::string& attributesString) const;
        void SaveAttributeToString(const Attribute& attribute, std::string& attributeString) const;
    };
}