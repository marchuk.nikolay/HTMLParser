#include "stdafx.h"
#include "DOMSaver.h"
#include "TagCreator.h"

using namespace HTMLParserLibrary;

void DOMSaver::Save(Element& root, std::string& content) const
{
    auto elements = root.GetChildren();

    for (auto& element : elements)
    {
        std::string children = "";
        if (element.GetName().empty())          //Save value element
        {
            content.append(element.GetValue());

            LOG_INFO << "Value element " << element.GetName() << " was saved.";
            continue;
        }

        TagCreator tagCreator;
        if (std::find(ParserConstants::SINGLE_TAG_ELEMENTS.begin(), ParserConstants::SINGLE_TAG_ELEMENTS.end(), element.GetName()) != ParserConstants::SINGLE_TAG_ELEMENTS.end())
        {
            std::string tag = "";              //Save single tag elements
            tagCreator.CreateOpenTag(element, tag);
            content.append(tag);

            LOG_INFO << "Signel tag element " << element.GetName() << " was saved.";
            continue;
        }

        std::string openTag = "";            //Save general elements
        tagCreator.CreateOpenTag(element, openTag);

        std::string closeTag = "";
        tagCreator.CreateCloseTag(element, closeTag);

        content.append(openTag);

        Save(element, children);
        content.append(children);

        content.append(closeTag);

        LOG_INFO << "Default element " << element.GetName() << " was saved.";
    }
}