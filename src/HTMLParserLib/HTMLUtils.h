#pragma once

#include "Attribute.h"

namespace HTMLParserLibrary
{
    void RemoveStyleElements(std::string& str);
    void RemoveScriptElements(std::string& str);
    void RemoveComments(std::string& str);
    void RemoveElements(std::string& str, const std::string& openTag, const std::string& closeTag);
    void RemoveHTMLElements(std::string& str);

    bool IsStringContainsHTML(const std::string& str);

    void ParseAttributes(const std::string& content, std::vector<Attribute>& attributes);
}