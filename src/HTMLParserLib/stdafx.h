// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers

#include <string>
#include <vector>
#include <algorithm>
#include <memory>
#include <regex>
#include <fstream>
#include <stack>
#include <exception>

#include "HTMLParsingException.h"
#include "ParserConstants.h"
#include "plog/Log.h"