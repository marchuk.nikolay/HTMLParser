#pragma once

#include "Element.h"

namespace HTMLParserLibrary
{
    class DOMSaver
    {
    public:
        void Save(Element& root, std::string& content) const;
    };
}