#include "stdafx.h"
#include "DefaultElementParsingStrategy.h"
#include "StringUtils.h"
#include "HTMLUtils.h"

using namespace HTMLParserLibrary;

void DefaultElementParsingStrategy::Parse(const std::string& content, size_t& pos, Element& element)
{
    LOG_INFO << "Begin";

    size_t whiteSpacePos = content.find(ParserConstants::WHITE_SPACE, pos);
    size_t greaterThanSignPos = content.find(ParserConstants::GREATER_THAN_SIGN, pos);

    std::string name;
    std::vector<Attribute> attributes;

    if (whiteSpacePos < greaterThanSignPos)                             //Open tag with attributes
    {
        name = content.substr(pos + 1, whiteSpacePos - pos - 1);
        std::string attributesString = content.substr(whiteSpacePos + 1, greaterThanSignPos - whiteSpacePos - 1);
        ParseAttributes(attributesString, attributes);
    }
    else                                                               //Open tag
    {
        name = content.substr(pos + 1, greaterThanSignPos - pos - 1);
    }

    element.SetName(name);

    if (!attributes.empty())
    {
        element.AddAttributes(attributes);
    }

    pos = greaterThanSignPos + 1;

    LOG_VERBOSE << "End";
}