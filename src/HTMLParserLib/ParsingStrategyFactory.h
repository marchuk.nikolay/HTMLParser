#pragma once

#include "IParsingStrategy.h"

namespace HTMLParserLibrary
{
    enum class ParsingType
    {
        ValueElement,
        SingleTagElement,
        DefaultElement
    };

    class ParsingStrategyFactory
    {
    public:
        ParsingStrategyPtr GetStrategy(const ParsingType& parsingType);
    };
}