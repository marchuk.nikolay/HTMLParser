#include "stdafx.h"
#include "TagCreator.h"

using namespace HTMLParserLibrary;

void TagCreator::CreateOpenTag(Element& element, std::string& tag) const
{
    tag = ParserConstants::LESSER_THAN_SIGN + element.GetName();

    if (element.GetAttributes().empty())
    {
        tag += ParserConstants::GREATER_THAN_SIGN;
        return;
    }

    std::string attributes = "";
    SaveAttributesToString(element.GetAttributes(), attributes);

    tag += attributes;
    tag += ParserConstants::GREATER_THAN_SIGN;
}

void TagCreator::CreateCloseTag(const Element& element, std::string& tag) const
{
    tag += ParserConstants::LESSER_THAN_SIGN;
    tag += ParserConstants::SLASH + element.GetName() + ParserConstants::GREATER_THAN_SIGN;
}

void TagCreator::SaveAttributesToString(const std::vector<Attribute>& attributes, std::string& attributesString) const
{
    for (auto& attribute : attributes)
    {
        std::string attributeString = "";
        SaveAttributeToString(attribute, attributeString);

        attributesString += ParserConstants::WHITE_SPACE + attributeString;
    }
}

void TagCreator::SaveAttributeToString(const Attribute& attribute, std::string& attributeString) const
{
    attributeString += attribute.GetName();

    if (!attribute.GetValue().empty())
    {
        attributeString += ParserConstants::EQUAL_SIGN;
        attributeString += ParserConstants::QUOTE;
        attributeString += attribute.GetValue();
        attributeString += ParserConstants::QUOTE;
    }
}