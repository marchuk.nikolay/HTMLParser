#pragma once
#include "stdafx.h"

namespace HTMLParserLibrary
{
    class HTMLParsingException: public std::exception
    {
    public:
        explicit HTMLParsingException(const std::string& message);
    };
}