#pragma once

#include "stdafx.h"
#include "DOMBuilder.h"

namespace HTMLParserLibrary
{
    class DOMParser
    {
    public:
        void ParseElements(const std::string& documentContent, std::vector<Element>& elements);

    private:
        void RemoveUnnecessaryElements(std::string& content) const;

    private:
        DOMBuilder domBuilder_;
    };
}