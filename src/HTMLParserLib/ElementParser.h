#pragma once
#include "IParsingStrategy.h"

namespace HTMLParserLibrary
{
    class ElementParser
    {
    public:
        ElementParser(ParsingStrategyPtr parsingStrategy);
        void Parse(const std::string& content, size_t& pos, Element& element);

    private:
        ParsingStrategyPtr parsingStrategy_;
    };
}