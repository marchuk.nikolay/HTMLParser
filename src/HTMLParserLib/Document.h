#pragma once

#include "stdafx.h"
#include "Element.h"

namespace HTMLParserLibrary
{
    class Document
    {
    public:
        std::shared_ptr<Element>& Load(const std::string& path);
        void Save(const std::string& path);

    private:
        std::shared_ptr<Element> rootElement_;
    };
}