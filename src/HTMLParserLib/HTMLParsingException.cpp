#include "stdafx.h"
#include "HTMLParsingException.h"

using namespace HTMLParserLibrary;

HTMLParsingException::HTMLParsingException(const std::string& message)
    : std::exception(message.c_str())
{
}