#include "stdafx.h"
#include "Document.h"
#include "DOMParser.h"
#include "DOMSaver.h"

using namespace HTMLParserLibrary;

std::shared_ptr<Element>& Document::Load(const std::string& path)
{
    plog::init(plog::info, "../../HTMLParsingLibraryLog.txt");

    std::ifstream file(path);

    if (!file)
    {
        LOG_FATAL << "Incorrect file path for document loading.";
        throw HTMLParsingException("Incorrect file path for document loading.");
    }
    std::string content((std::istreambuf_iterator<char>(file)), (std::istreambuf_iterator<char>()));

    DOMParser parser;
    std::vector<Element> elements;
    parser.ParseElements(content, elements);

    rootElement_.reset(new Element);
    rootElement_->AddChildren(elements);

    return rootElement_;
}

void Document::Save(const std::string& path)
{
    if (rootElement_ == nullptr)
    {
        LOG_FATAL << "Please, load document before saving.";
        throw HTMLParsingException("Please, load document before saving.");
    }

    DOMSaver saver;
    std::string content = "";
    saver.Save(*rootElement_, content);

    std::ofstream file(path);
    file << content;
    file.close();
}