// HTMLParser.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Document.h"

void IncorrectDocumentLoading();
void CorrectDocumentLoading();

using namespace HTMLParserLibrary;

int main()
{
    try
    {
        CorrectDocumentLoading();
    }
    catch (const HTMLParsingException& ex)
    {
        std::cout << ex.what() << "\n";
    }
    catch (const std::exception& ex)
    {
        std::cout << ex.what() << "\n";
    }

    return 0;
}

void IncorrectDocumentLoading()
{
    Document document;
    auto root = document.Load("../../incorrect.html");
}

void CorrectDocumentLoading()
{
    Document document;
    auto root = document.Load("../../correct.html");
}