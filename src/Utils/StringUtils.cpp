#include "stdafx.h"
#include "StringUtils.h"

void Utils::SplitString(const std::string& str, char delimiter, std::vector<std::string>& tokens)
{
    std::stringstream stream(str);
    std::string token;

    while (std::getline(stream, token, delimiter))
    {
        tokens.push_back(token);
    }
}

void Utils::ReplaceAllEntities(std::string& str, const std::string& findStr, const std::string& replaceStr)
{
    size_t pos = 0;

    while ((pos = str.find(findStr, pos)) != std::string::npos)
    {
        str.replace(pos, findStr.size(), replaceStr);
        pos += replaceStr.size();
    }
}