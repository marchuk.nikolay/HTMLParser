#pragma once
#include "stdafx.h"

namespace Utils
{
    void SplitString(const std::string& str, char delimiter, std::vector<std::string>& tokens);

    void ReplaceAllEntities(std::string& str, const std::string& findStr, const std::string& replaceStr);
}