#pragma once

#include "cppunit\TestFixture.h"
#include "cppunit\extensions\HelperMacros.h"

class StringUtilsTest : public CppUnit::TestFixture
{
public:
    void TestSplitString();
    void TestOneItem();
    void TestReplaceAllEntities();
    void TestReplaceAllEntitiesEmptyString();
    void TestReplaceInStrWithoutFindStr();

    CPPUNIT_TEST_SUITE(StringUtilsTest);

    CPPUNIT_TEST(TestSplitString);
    CPPUNIT_TEST(TestOneItem);
    CPPUNIT_TEST(TestReplaceAllEntities);
    CPPUNIT_TEST(TestReplaceAllEntitiesEmptyString);
    CPPUNIT_TEST(TestReplaceInStrWithoutFindStr);

    CPPUNIT_TEST_SUITE_END();
};

