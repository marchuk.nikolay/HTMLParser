#include "stdafx.h"
#include "StringUtilsTest.h"
#include "StringUtils.h"

using namespace Utils;

void StringUtilsTest::TestSplitString()
{
    std::string str("test test1 test2");
    std::vector<std::string> tokens;
    const char DELIMITER = ' ';

    SplitString(str, DELIMITER, tokens);

    CPPUNIT_ASSERT(tokens.size() == 3);

    CPPUNIT_ASSERT(tokens.at(0) == "test");
    CPPUNIT_ASSERT(tokens.at(1) == "test1");
    CPPUNIT_ASSERT(tokens.at(2) == "test2");
}

void StringUtilsTest::TestOneItem()
{
    std::string str("test");
    std::vector<std::string> tokens;
    const char DELIMITER = ' ';

    SplitString(str, DELIMITER, tokens);

    CPPUNIT_ASSERT(tokens.size() == 1);

    CPPUNIT_ASSERT(tokens.at(0) == "test");
}

void StringUtilsTest::TestReplaceAllEntities()
{
    std::string str("test=>test=>test");
    std::string findStr("=>");
    std::string replaceStr("<=");

    ReplaceAllEntities(str, findStr, replaceStr);

    CPPUNIT_ASSERT(str == "test<=test<=test");
}

void StringUtilsTest::TestReplaceAllEntitiesEmptyString()
{
    std::string str("");
    std::string findStr("=>");
    std::string replaceStr("<=");

    ReplaceAllEntities(str, findStr, replaceStr);

    CPPUNIT_ASSERT(str.empty());
}

void StringUtilsTest::TestReplaceInStrWithoutFindStr()
{
    std::string str("test<=test<=test");
    std::string findStr("=>");
    std::string replaceStr("<=");

    ReplaceAllEntities(str, findStr, replaceStr);

    CPPUNIT_ASSERT(str == "test<=test<=test");
}