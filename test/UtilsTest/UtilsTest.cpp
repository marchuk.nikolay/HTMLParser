// UtilsTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "cppunit\ui\text\TestRunner.h"
#include "cppunit\extensions\TestFactoryRegistry.h"

#include "StringUtilsTest.h"
CPPUNIT_TEST_SUITE_REGISTRATION(StringUtilsTest);

int main()
{
    CppUnit::TextUi::TestRunner runner;
    CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry();
    runner.addTest(registry.makeTest());
    runner.run();

    return 0;
}

