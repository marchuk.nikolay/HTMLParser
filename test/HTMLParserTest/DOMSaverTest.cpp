#include "stdafx.h"
#include "DOMSaverTest.h"
#include "DOMSaver.h"

using namespace HTMLParserLibrary;

void DOMSaverTest::TestSaveSingleElement()
{
    Element element("div");

    Element root;
    root.AddToEnd(element);

    std::string content = "";

    DOMSaver saver;
    saver.Save(root, content);

    CPPUNIT_ASSERT(content == "<div></div>");
}

void DOMSaverTest::TestSaveDOMWithoutNestingAndAttributes()
{
    Element header("h1");
    Element paragraph("p");
    Element div("div");

    Element root;
    root.AddToEnd(header);
    root.AddToEnd(paragraph);
    root.AddToEnd(div);

    std::string content = "";

    DOMSaver saver;
    saver.Save(root, content);

    CPPUNIT_ASSERT(content == "<h1></h1><p></p><div></div>");
}

void DOMSaverTest::TestSaveElementWithAttributes()
{
    Attribute disabled("disabled");
    Attribute classAttribute("class", "headers");

    Element element("h1");
    element.AddAttribute(disabled);
    element.AddAttribute(classAttribute);

    Element root;
    root.AddToEnd(element);

    std::string content = "";

    DOMSaver saver;
    saver.Save(root, content);

    CPPUNIT_ASSERT(content == "<h1 disabled class=\"headers\"></h1>");
}

void DOMSaverTest::TestSaveElementWithNesting()
{
    Element div("div");
    Element header("h1");
    div.AddToEnd(header);

    Element root;
    root.AddToEnd(div);

    std::string content = "";

    DOMSaver saver;
    saver.Save(root, content);

    CPPUNIT_ASSERT(content == "<div><h1></h1></div>");
}

void DOMSaverTest::TestSaveElementWithValueAndAttributes()
{
    Attribute disabled("disabled");
    Attribute classAttribute("class", "headers");

    Element element("h1");
    element.AddAttribute(disabled);
    element.AddAttribute(classAttribute);

    Element value;
    value.SetValue("Test header");
    element.AddToEnd(value);

    Element root;
    root.AddToEnd(element);

    std::string content = "";

    DOMSaver saver;
    saver.Save(root, content);

    CPPUNIT_ASSERT(content == "<h1 disabled class=\"headers\">Test header</h1>");
}

void DOMSaverTest::TestSaveElementWithFewValues()
{
    Element header("h1");
    Element headerText;
    headerText.SetValue("Test");
    header.AddToEnd(headerText);

    Element textElement1;
    textElement1.SetValue("My ");

    Element textElement2;
    textElement2.SetValue(" header");

    Element div("div");
    std::vector<Element> divChildren = {textElement1, header, textElement2};
    div.AddChildren(divChildren);

    Element root;
    root.AddToEnd(div);

    std::string content = "";

    DOMSaver saver;
    saver.Save(root, content);

    CPPUNIT_ASSERT(content == "<div>My <h1>Test</h1> header</div>");
}

void DOMSaverTest::TestSaveElementWithNestingAndAttributes()
{
    Element header("h1");
    Element headerText;
    headerText.SetValue("First Header");
    header.AddToEnd(headerText);

    Attribute headerAttribute("class", "headers");
    header.AddAttribute(headerAttribute);

    Element div("div");
    div.AddToEnd(header);

    Attribute divAttribute("disabled");
    div.AddAttribute(divAttribute);

    Element root;
    root.AddToEnd(div);

    std::string content = "";

    DOMSaver saver;
    saver.Save(root, content);

    CPPUNIT_ASSERT(content == "<div disabled><h1 class=\"headers\">First Header</h1></div>");
}

void DOMSaverTest::TestSaveSingleTagElements()
{
    Element header("h1");
    Element headerText;
    headerText.SetValue("Test header");
    header.AddToEnd(headerText);

    Element emptyLine("br");

    Element paragraph("p");
    Element paragraphText;
    paragraphText.SetValue("Test paragraph");
    paragraph.AddToEnd(paragraphText);

    Element line("hr");

    std::vector<Element> elements = {header, emptyLine, paragraph, line};

    Element root;
    root.AddChildren(elements);

    std::string content = "";

    DOMSaver saver;
    saver.Save(root, content);

    CPPUNIT_ASSERT(content == "<h1>Test header</h1><br><p>Test paragraph</p><hr>");
}