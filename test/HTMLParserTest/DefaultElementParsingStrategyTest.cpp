#include "stdafx.h"
#include "DefaultElementParsingStrategyTest.h"
#include "DefaultElementParsingStrategy.h"

using namespace HTMLParserLibrary;

void DefaultElementParsingStrategyTest::TestParse()
{
    std::string content("<div>Test</div>");
    size_t pos = 0;

    Element element;
    DefaultElementParsingStrategy strategy;
    strategy.Parse(content, pos, element);

    CPPUNIT_ASSERT(element.GetName() == "div");
    CPPUNIT_ASSERT(element.GetValue().empty());
}

void DefaultElementParsingStrategyTest::TestParseWithAttributes()
{
    std::string content("<h1 class=\"headers\" disabled>Test</h1>");
    size_t pos = 0;

    Element element;
    DefaultElementParsingStrategy strategy;
    strategy.Parse(content, pos, element);

    CPPUNIT_ASSERT(element.GetName() == "h1");
    CPPUNIT_ASSERT(element.GetValue().empty());

    CPPUNIT_ASSERT(element.GetAttributes().size() == 2);

    CPPUNIT_ASSERT(element.GetAttributes().at(0).GetName() == "class");
    CPPUNIT_ASSERT(element.GetAttributes().at(0).GetValue() == "headers");

    CPPUNIT_ASSERT(element.GetAttributes().at(1).GetName() == "disabled");
    CPPUNIT_ASSERT(element.GetAttributes().at(1).GetValue().empty());
}