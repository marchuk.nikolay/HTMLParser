#pragma once

#include "cppunit\TestFixture.h"
#include "cppunit\extensions\HelperMacros.h"

class DocumentTest : public CppUnit::TestFixture
{
public:
    void TestDocumentLoading();
    void TestDocumentLoadingIncorrectPath();
    void TestEmptyDocumentLoading();
    void TestDocumentSaving();
    void TestDocumentSavingFromEmptyDOM();

    CPPUNIT_TEST_SUITE(DocumentTest);

    CPPUNIT_TEST(TestDocumentLoading);
    CPPUNIT_TEST(TestDocumentLoadingIncorrectPath);
    CPPUNIT_TEST(TestEmptyDocumentLoading);
    CPPUNIT_TEST(TestDocumentSaving);
    CPPUNIT_TEST(TestDocumentSavingFromEmptyDOM);

    CPPUNIT_TEST_SUITE_END();
};

