#include "stdafx.h"
#include "ValueElementParsingStrategyTest.h"
#include "ValueElementParsingStrategy.h"

using namespace HTMLParserLibrary;

void ValueElementParsingStrategyTest::TestParse()
{
    std::string content("<div>Test</div>");
    size_t pos = 5;

    Element value;
    ValueElementParsingStrategy strategy;
    strategy.Parse(content, pos, value);

    CPPUNIT_ASSERT(value.GetName().empty());
    CPPUNIT_ASSERT(value.GetValue() == "Test");
}

void ValueElementParsingStrategyTest::TestParseIncorrectDocument()
{
    std::string content("<div>Test");
    size_t pos = 5;

    Element value;
    ValueElementParsingStrategy strategy;
    CPPUNIT_ASSERT_THROW(strategy.Parse(content, pos, value), HTMLParsingException);
}