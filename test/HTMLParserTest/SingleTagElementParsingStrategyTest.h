#pragma once

#include "cppunit\TestFixture.h"
#include "cppunit\extensions\HelperMacros.h"

class SingleTagElementParsingStrategyTest : public CppUnit::TestFixture
{
public:
    void TestParse();
    void TestParseWithAttributes();
    void TestParseCloseTagElementWithAttributes();
    void TestParseSelfClosedTagElementWithAttributes();
    void TestParseSelfClosedWhitespaceTagElementWithAttributes();
    void TestParseInvalidElement();
    void TestParseDoctypeHTML4();
    void TestParseDoctypeHTML5();

    CPPUNIT_TEST_SUITE(SingleTagElementParsingStrategyTest);

    CPPUNIT_TEST(TestParse);
    CPPUNIT_TEST(TestParseWithAttributes);
    CPPUNIT_TEST(TestParseCloseTagElementWithAttributes);
    CPPUNIT_TEST(TestParseSelfClosedTagElementWithAttributes);
    CPPUNIT_TEST(TestParseSelfClosedWhitespaceTagElementWithAttributes);
    CPPUNIT_TEST(TestParseInvalidElement);
    CPPUNIT_TEST(TestParseDoctypeHTML4);
    CPPUNIT_TEST(TestParseDoctypeHTML5);

    CPPUNIT_TEST_SUITE_END();
};