#pragma once

#include "cppunit\TestFixture.h"
#include "cppunit\extensions\HelperMacros.h"

class AttributeTest : public CppUnit::TestFixture
{
public:
    void TestGetName();
    void TestGetValue();
    void TestSetName();
    void TestSetValue();
    void TestSetItem();
    void TestAttributeWithoutValue();

    CPPUNIT_TEST_SUITE(AttributeTest);

    CPPUNIT_TEST(TestGetName);
    CPPUNIT_TEST(TestGetValue);
    CPPUNIT_TEST(TestSetName);
    CPPUNIT_TEST(TestSetValue);
    CPPUNIT_TEST(TestSetItem);
    CPPUNIT_TEST(TestAttributeWithoutValue);

    CPPUNIT_TEST_SUITE_END();
};

