#pragma once

#include "cppunit\TestFixture.h"
#include "cppunit\extensions\HelperMacros.h"

class ParsingStrategyFactoryTest : public CppUnit::TestFixture
{
public:
    void TestGetValueElementStrategy();
    void TestGetSingleTagElementStrategy();
    void TestGetDefaultElementStrategy();
    void TestUnknwonStrategy();

    CPPUNIT_TEST_SUITE(ParsingStrategyFactoryTest);

    CPPUNIT_TEST(TestGetValueElementStrategy);
    CPPUNIT_TEST(TestGetSingleTagElementStrategy);
    CPPUNIT_TEST(TestGetDefaultElementStrategy);
    CPPUNIT_TEST(TestUnknwonStrategy);

    CPPUNIT_TEST_SUITE_END();
};