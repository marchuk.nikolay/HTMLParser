#pragma once

#include "cppunit\TestFixture.h"
#include "cppunit\extensions\HelperMacros.h"

class TagCreatorTest : public CppUnit::TestFixture
{
public:
    void TestCreateOpenTag();
    void TestCreateOpenTagWithAttributes();
    void TestCreateCloseTag();

    CPPUNIT_TEST_SUITE(TagCreatorTest);

    CPPUNIT_TEST(TestCreateOpenTag);
    CPPUNIT_TEST(TestCreateOpenTagWithAttributes);
    CPPUNIT_TEST(TestCreateCloseTag);

    CPPUNIT_TEST_SUITE_END();
};