#include "stdafx.h"
#include "DocumentTest.h"
#include "Document.h"
#include "StringUtils.h"

using namespace HTMLParserLibrary;

void DocumentTest::TestDocumentLoading()
{
    Document document;
    auto root = document.Load("../TestFiles/test.html");

    auto children = root->GetChildren();

    CPPUNIT_ASSERT(children.size() == 2);

    CPPUNIT_ASSERT(children.at(0).GetName() == "head");
    CPPUNIT_ASSERT(children.at(0).GetValue().empty());

    CPPUNIT_ASSERT(children.at(1).GetName() == "body");
    CPPUNIT_ASSERT(children.at(1).GetValue().empty());

    auto bodyChildren = children.at(1).GetChildren();

    CPPUNIT_ASSERT(bodyChildren.size() == 1);

    CPPUNIT_ASSERT(bodyChildren.at(0).GetName() == "h1");
    CPPUNIT_ASSERT(bodyChildren.at(0).GetValue().empty());

    auto headerAttributes = bodyChildren.at(0).GetAttributes();

    CPPUNIT_ASSERT(headerAttributes.size() == 1);

    CPPUNIT_ASSERT(headerAttributes.at(0).GetName() == "class");
    CPPUNIT_ASSERT(headerAttributes.at(0).GetValue() == "headers");

    auto headerChildren = bodyChildren.at(0).GetChildren();

    CPPUNIT_ASSERT(headerChildren.size() == 1);

    CPPUNIT_ASSERT(headerChildren.at(0).GetName().empty());
    CPPUNIT_ASSERT(headerChildren.at(0).GetValue() == "Header");
}

void DocumentTest::TestDocumentLoadingIncorrectPath()
{
    Document document;
    CPPUNIT_ASSERT_THROW(document.Load("../TestFiles/text.html"), HTMLParsingException);
}

void DocumentTest::TestEmptyDocumentLoading()
{
    Document document;
    auto root = document.Load("../TestFiles/empty.html");

    auto children = root->GetChildren();

    CPPUNIT_ASSERT(children.empty());
}

void DocumentTest::TestDocumentSaving()
{
    Document document;

    std::ifstream testFile("../TestFiles/test.html");
    std::string testFileContent((std::istreambuf_iterator<char>(testFile)), (std::istreambuf_iterator<char>()));

    document.Load("../TestFiles/test.html");
    document.Save("../TestFiles/saved.html");

    std::ifstream savedFile("../TestFiles/saved.html");
    std::string savedFileContent((std::istreambuf_iterator<char>(savedFile)), (std::istreambuf_iterator<char>()));

    Utils::ReplaceAllEntities(testFileContent, "\n", "");
    Utils::ReplaceAllEntities(testFileContent, "\t", "");

    Utils::ReplaceAllEntities(savedFileContent, "\n", "");
    Utils::ReplaceAllEntities(savedFileContent, "\t", "");

    CPPUNIT_ASSERT(testFileContent == savedFileContent);
}

void DocumentTest::TestDocumentSavingFromEmptyDOM()
{
    Document document;
    CPPUNIT_ASSERT_THROW(document.Save("../TestFiles/saved.html"), HTMLParsingException);
}