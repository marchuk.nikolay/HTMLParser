#pragma once

#include "cppunit\TestFixture.h"
#include "cppunit\extensions\HelperMacros.h"

class DefaultElementParsingStrategyTest : public CppUnit::TestFixture
{
public:
    void TestParse();
    void TestParseWithAttributes();

    CPPUNIT_TEST_SUITE(DefaultElementParsingStrategyTest);

    CPPUNIT_TEST(TestParse);
    CPPUNIT_TEST(TestParseWithAttributes);

    CPPUNIT_TEST_SUITE_END();
};