#include "stdafx.h"
#include "HTMLUtilsTest.h"
#include "HTMLUtils.h"

using namespace HTMLParserLibrary;

void HTMLUtilsTest::TestRemoveStyleElements()
{
    std::string content = "<style>p {color: blue;}</style><h1>Test header</h1><p>Test paragraph</p>";
    RemoveStyleElements(content);

    CPPUNIT_ASSERT(content == "<h1>Test header</h1><p>Test paragraph</p>");
}

void HTMLUtilsTest::TestRemoveStyleElementsWithAttributes()
{
    std::string content = "<style id=\"header-style\">p {color: blue;}</style><h1>Test header</h1><p>Test paragraph</p>";
    RemoveStyleElements(content);

    CPPUNIT_ASSERT(content == "<h1>Test header</h1><p>Test paragraph</p>");
}

void HTMLUtilsTest::TestRemoveScriptElements()
{
    std::string content = "<script>console.log('Message');</script><h1>Test header</h1><p>Test paragraph</p>";
    RemoveScriptElements(content);

    CPPUNIT_ASSERT(content == "<h1>Test header</h1><p>Test paragraph</p>");
}

void HTMLUtilsTest::TestRemoveScriptElementsWithAttributes()
{
    std::string content = "<script src=\"https://assets.gitlab-static.net/assets/webpack/webpack_runtime.de43f9ef5459cc1f7d4e.bundle.js\" defer=\"defer\"></script><h1>Test header</h1><p>Test paragraph</p>";
    RemoveScriptElements(content);

    CPPUNIT_ASSERT(content == "<h1>Test header</h1><p>Test paragraph</p>");
}

void HTMLUtilsTest::TestRemoveComments()
{
    std::string content = "<!-- Header --><h1>Test header</h1><!-- Header -->";
    RemoveComments(content);

    CPPUNIT_ASSERT(content == "<h1>Test header</h1>");
}

void HTMLUtilsTest::TestRemoveElements()
{
    std::string content = "<h1>Test header</h1><p>Test paragraph</p>";
    RemoveElements(content, "<h1>", "</h1>");

    CPPUNIT_ASSERT(content == "<p>Test paragraph</p>");
}

void HTMLUtilsTest::TestRemoveElementsOnlyOpenTag()
{
    std::string content = "<h1>Test header<p>Test paragraph</p>";

    CPPUNIT_ASSERT_THROW(RemoveElements(content, "<h1>", "</h1>"), HTMLParsingException);
}

void HTMLUtilsTest::TestRemoveElementsOnlyCloseTag()
{
    std::string content = "Test header</h1><p>Test paragraph</p>";

    CPPUNIT_ASSERT_THROW(RemoveElements(content, "<h1>", "</h1>"), HTMLParsingException);
}

void HTMLUtilsTest::TestRemoveElementsNoElements()
{
    std::string content = "<p>Test paragraph</p>";
    RemoveElements(content, "<h1>", "</h1>");

    CPPUNIT_ASSERT(content == "<p>Test paragraph</p>");
}

void HTMLUtilsTest::TestRemoveElementsFewEntities()
{
    std::string content = "<h1>Test header</h1><p>Test paragraph 1</p><p>Test paragraph 2</p>";
    RemoveElements(content, "<p>", "</p>");

    CPPUNIT_ASSERT(content == "<h1>Test header</h1>");
}

void HTMLUtilsTest::TestRemoveHTMLElements()
{
    std::string str("<div>Test <b>bold <b/><h1>header</h1></div>");
    RemoveHTMLElements(str);

    CPPUNIT_ASSERT(str == "Test bold header");
}

void HTMLUtilsTest::TestRemoveHTMLElementsEmptyString()
{
    std::string str("");
    RemoveHTMLElements(str);

    CPPUNIT_ASSERT(str.empty());
}

void HTMLUtilsTest::TestRemoveHTMLElementsWithoutHTML()
{
    std::string str("Test bold header");
    RemoveHTMLElements(str);

    CPPUNIT_ASSERT(str == "Test bold header");
}

void HTMLUtilsTest::TestRemoveHTMLElementsInvalidHTMLString()
{
    std::string str("<h1 Test bold header");
    CPPUNIT_ASSERT_THROW(RemoveHTMLElements(str), HTMLParsingException);
}

void HTMLUtilsTest::TestIsStringContainsHTML()
{
    std::string str("<h1> Test bold header</h1>");
    CPPUNIT_ASSERT(IsStringContainsHTML(str));
}

void HTMLUtilsTest::TestStringDoesntContainHTML()
{
    std::string str("Test bold header");
    CPPUNIT_ASSERT(!IsStringContainsHTML(str));
}

void HTMLUtilsTest::TestIsStringContainsHTMLFewElements()
{
    std::string str("<div>Test <b>bold <b/><h1>header</h1></div>");
    CPPUNIT_ASSERT(IsStringContainsHTML(str));
}

void HTMLUtilsTest::TestIsStringContainsHTMLEmptyString()
{
    std::string str("");
    CPPUNIT_ASSERT(!IsStringContainsHTML(str));
}

void HTMLUtilsTest::TestParseSingleAttributeWithoutValue()
{
    std::string str("disabled");
    std::vector<Attribute> attributes;

    ParseAttributes(str, attributes);

    CPPUNIT_ASSERT(attributes.size() == 1);

    CPPUNIT_ASSERT(attributes.at(0).GetName() == "disabled");
    CPPUNIT_ASSERT(attributes.at(0).GetValue().empty());
}

void HTMLUtilsTest::TestParseFewAttributesWithoutValue()
{
    std::string str("disabled disabled");
    std::vector<Attribute> attributes;

    ParseAttributes(str, attributes);

    CPPUNIT_ASSERT(attributes.size() == 2);

    CPPUNIT_ASSERT(attributes.at(0).GetName() == "disabled");
    CPPUNIT_ASSERT(attributes.at(0).GetValue().empty());

    CPPUNIT_ASSERT(attributes.at(1).GetName() == "disabled");
    CPPUNIT_ASSERT(attributes.at(1).GetValue().empty());
}

void HTMLUtilsTest::TestParseAttributeWithoutValueAndDefaultAttribute()
{
    std::string str("disabled class=\"headers\"");
    std::vector<Attribute> attributes;

    ParseAttributes(str, attributes);

    CPPUNIT_ASSERT(attributes.size() == 2);

    CPPUNIT_ASSERT(attributes.at(0).GetName() == "disabled");
    CPPUNIT_ASSERT(attributes.at(0).GetValue().empty());

    CPPUNIT_ASSERT(attributes.at(1).GetName() == "class");
    CPPUNIT_ASSERT(attributes.at(1).GetValue() == "headers");
}

void HTMLUtilsTest::TestParseDefaultAttributeAndAttributeWithoutValue()
{
    std::string str("class=\"headers\" disabled");
    std::vector<Attribute> attributes;

    ParseAttributes(str, attributes);

    CPPUNIT_ASSERT(attributes.size() == 2);

    CPPUNIT_ASSERT(attributes.at(0).GetName() == "class");
    CPPUNIT_ASSERT(attributes.at(0).GetValue() == "headers");

    CPPUNIT_ASSERT(attributes.at(1).GetName() == "disabled");
    CPPUNIT_ASSERT(attributes.at(1).GetValue().empty());
}

void HTMLUtilsTest::TestParseFewDefaultAttributes()
{
    std::string str("class=\"headers\" id=\"first-header\"");
    std::vector<Attribute> attributes;

    ParseAttributes(str, attributes);

    CPPUNIT_ASSERT(attributes.size() == 2);

    CPPUNIT_ASSERT(attributes.at(0).GetName() == "class");
    CPPUNIT_ASSERT(attributes.at(0).GetValue() == "headers");

    CPPUNIT_ASSERT(attributes.at(1).GetName() == "id");
    CPPUNIT_ASSERT(attributes.at(1).GetValue() == "first-header");
}

void HTMLUtilsTest::TestParseAttributeWithWhiteSpaces()
{
    std::string str("d=\"M18, 34.38 3, 14 33, 14 Z\"");
    std::vector<Attribute> attributes;

    ParseAttributes(str, attributes);

    CPPUNIT_ASSERT(attributes.size() == 1);

    CPPUNIT_ASSERT(attributes.at(0).GetName() == "d");
    CPPUNIT_ASSERT(attributes.at(0).GetValue() == "M18, 34.38 3, 14 33, 14 Z");
}