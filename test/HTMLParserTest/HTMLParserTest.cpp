// HTMLParserTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "cppunit\ui\text\TestRunner.h"
#include "cppunit\extensions\TestFactoryRegistry.h"

#include "AttributeTest.h"
CPPUNIT_TEST_SUITE_REGISTRATION(AttributeTest);

#include "ElementTest.h"
CPPUNIT_TEST_SUITE_REGISTRATION(ElementTest);

#include "DOMParserTest.h"
CPPUNIT_TEST_SUITE_REGISTRATION(DOMParserTest);

#include "DOMSaverTest.h"
CPPUNIT_TEST_SUITE_REGISTRATION(DOMSaverTest);

#include "DocumentTest.h"
CPPUNIT_TEST_SUITE_REGISTRATION(DocumentTest);

#include "TagCreatorTest.h"
CPPUNIT_TEST_SUITE_REGISTRATION(TagCreatorTest);

#include "HTMLUtilsTest.h"
CPPUNIT_TEST_SUITE_REGISTRATION(HTMLUtilsTest);

#include "DOMBuilderTest.h"
CPPUNIT_TEST_SUITE_REGISTRATION(DOMBuilderTest);

#include "ValueElementParsingStrategyTest.h"
CPPUNIT_TEST_SUITE_REGISTRATION(ValueElementParsingStrategyTest);

#include "SingleTagElementParsingStrategyTest.h"
CPPUNIT_TEST_SUITE_REGISTRATION(SingleTagElementParsingStrategyTest);

#include "DefaultElementParsingStrategyTest.h"
CPPUNIT_TEST_SUITE_REGISTRATION(DefaultElementParsingStrategyTest);

#include "ElementParserTest.h"
CPPUNIT_TEST_SUITE_REGISTRATION(ElementParserTest);

#include "ParsingStrategyFactoryTest.h"
CPPUNIT_TEST_SUITE_REGISTRATION(ParsingStrategyFactoryTest);

int main()
{
    CppUnit::TextUi::TestRunner runner;
    CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry();
    runner.addTest(registry.makeTest());
    runner.run();

    return 0;
}