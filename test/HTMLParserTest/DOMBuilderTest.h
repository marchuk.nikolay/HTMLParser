#pragma once

#include "cppunit\TestFixture.h"
#include "cppunit\extensions\HelperMacros.h"

class DOMBuilderTest : public CppUnit::TestFixture
{
public:
    void TestInsertElement();
    void TestInsertElementInIncorrectPlace();
    void TestInsertElementNesting();
    void TestGetDOM();

    CPPUNIT_TEST_SUITE(DOMBuilderTest);

    CPPUNIT_TEST(TestInsertElement);
    CPPUNIT_TEST(TestInsertElementInIncorrectPlace);
    CPPUNIT_TEST(TestInsertElementNesting);
    CPPUNIT_TEST(TestGetDOM);

    CPPUNIT_TEST_SUITE_END();
};