#pragma once

#include "cppunit\TestFixture.h"
#include "cppunit\extensions\HelperMacros.h"

class DOMParserTest : public CppUnit::TestFixture
{
public:
    void TestParseStringWithoutHTML();
    void TestParseSingleElement();
    void TestParseHTMLWithoutNestingAndAttributes();
    void TestParseHTMLWithAttributes();
    void TestParseNestedElement();
    void TestParseElementWithValue();
    void TestParseElementWithFewValues();
    void TestParseNestedElementsWithAttributes();
    void TestSkipStyleElements();
    void TestOnlyOpenStyleTag();
    void TestOnlyCloseStyleTag();
    void TestSkipScriptElements();
    void TestSkipComments();
    void TestSkipUnnecessaryElements();
    void TestStringWithSkippedElements();
    void TestParseSingleTagElements();
    void TestParseEmptyString();
    void TestParseStringWithTabsAndNewLines();
    void TestParseElementWithOnlyOpenTag();
    void TestParseElementWithOnlyCloseTag();
    void TestParseElementWithDifferentOpenAndCloseTags();
    void TestParseCloseTagBeforeOpen();
    void TestParseDifferentNumberOfOpenAndCloseTags();
    void TestParseElementWithIncorrectCloseTag();

    CPPUNIT_TEST_SUITE(DOMParserTest);

    CPPUNIT_TEST(TestParseStringWithoutHTML);
    CPPUNIT_TEST(TestParseSingleElement);
    CPPUNIT_TEST(TestParseHTMLWithoutNestingAndAttributes);
    CPPUNIT_TEST(TestParseHTMLWithAttributes);
    CPPUNIT_TEST(TestParseNestedElement);
    CPPUNIT_TEST(TestParseElementWithValue);
    CPPUNIT_TEST(TestParseElementWithFewValues);
    CPPUNIT_TEST(TestParseNestedElementsWithAttributes);
    CPPUNIT_TEST(TestSkipStyleElements);
    CPPUNIT_TEST(TestOnlyOpenStyleTag);
    CPPUNIT_TEST(TestOnlyCloseStyleTag);
    CPPUNIT_TEST(TestSkipScriptElements);
    CPPUNIT_TEST(TestSkipComments);
    CPPUNIT_TEST(TestSkipUnnecessaryElements);
    CPPUNIT_TEST(TestStringWithSkippedElements);
    CPPUNIT_TEST(TestParseSingleTagElements);
    CPPUNIT_TEST(TestParseEmptyString);
    CPPUNIT_TEST(TestParseStringWithTabsAndNewLines);
    CPPUNIT_TEST(TestParseElementWithOnlyOpenTag);
    CPPUNIT_TEST(TestParseElementWithOnlyCloseTag);
    CPPUNIT_TEST(TestParseElementWithDifferentOpenAndCloseTags);
    CPPUNIT_TEST(TestParseCloseTagBeforeOpen);
    CPPUNIT_TEST(TestParseDifferentNumberOfOpenAndCloseTags);
    CPPUNIT_TEST(TestParseElementWithIncorrectCloseTag);

    CPPUNIT_TEST_SUITE_END();
};

