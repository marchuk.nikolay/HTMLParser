#pragma once

#include "cppunit\TestFixture.h"
#include "cppunit\extensions\HelperMacros.h"

class HTMLUtilsTest : public CppUnit::TestFixture
{
public:
    void TestRemoveStyleElements();
    void TestRemoveStyleElementsWithAttributes();
    void TestRemoveScriptElements();
    void TestRemoveScriptElementsWithAttributes();
    void TestRemoveComments();
    void TestRemoveElements();
    void TestRemoveElementsOnlyOpenTag();
    void TestRemoveElementsOnlyCloseTag();
    void TestRemoveElementsNoElements();
    void TestRemoveElementsFewEntities();
    void TestRemoveHTMLElements();
    void TestRemoveHTMLElementsEmptyString();
    void TestRemoveHTMLElementsWithoutHTML();
    void TestRemoveHTMLElementsInvalidHTMLString();
    void TestIsStringContainsHTML();
    void TestStringDoesntContainHTML();
    void TestIsStringContainsHTMLFewElements();
    void TestIsStringContainsHTMLEmptyString();
    void TestParseSingleAttributeWithoutValue();
    void TestParseFewAttributesWithoutValue();
    void TestParseAttributeWithoutValueAndDefaultAttribute();
    void TestParseDefaultAttributeAndAttributeWithoutValue();
    void TestParseFewDefaultAttributes();
    void TestParseAttributeWithWhiteSpaces();

    CPPUNIT_TEST_SUITE(HTMLUtilsTest);

    CPPUNIT_TEST(TestRemoveStyleElements);
    CPPUNIT_TEST(TestRemoveStyleElementsWithAttributes);
    CPPUNIT_TEST(TestRemoveScriptElements);
    CPPUNIT_TEST(TestRemoveScriptElementsWithAttributes);
    CPPUNIT_TEST(TestRemoveComments);
    CPPUNIT_TEST(TestRemoveElements);
    CPPUNIT_TEST(TestRemoveHTMLElements);
    CPPUNIT_TEST(TestRemoveElementsOnlyOpenTag);
    CPPUNIT_TEST(TestRemoveElementsOnlyCloseTag);
    CPPUNIT_TEST(TestRemoveElementsNoElements);
    CPPUNIT_TEST(TestRemoveElementsFewEntities);
    CPPUNIT_TEST(TestRemoveHTMLElementsEmptyString);
    CPPUNIT_TEST(TestRemoveHTMLElementsWithoutHTML);
    CPPUNIT_TEST(TestRemoveHTMLElementsInvalidHTMLString);
    CPPUNIT_TEST(TestIsStringContainsHTML);
    CPPUNIT_TEST(TestStringDoesntContainHTML);
    CPPUNIT_TEST(TestIsStringContainsHTMLFewElements);
    CPPUNIT_TEST(TestIsStringContainsHTMLEmptyString);
    CPPUNIT_TEST(TestParseSingleAttributeWithoutValue);
    CPPUNIT_TEST(TestParseFewAttributesWithoutValue);
    CPPUNIT_TEST(TestParseAttributeWithoutValueAndDefaultAttribute);
    CPPUNIT_TEST(TestParseDefaultAttributeAndAttributeWithoutValue);
    CPPUNIT_TEST(TestParseFewDefaultAttributes);
    CPPUNIT_TEST(TestParseAttributeWithWhiteSpaces);

    CPPUNIT_TEST_SUITE_END();
};