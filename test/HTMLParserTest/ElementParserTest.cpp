#include "stdafx.h"
#include "ElementParserTest.h"
#include "ElementParser.h"
#include "ParsingStrategyFactory.h"

using namespace HTMLParserLibrary;

void ElementParserTest::TestParseValueElement()
{
    std::string content("<div>Test</div>");
    size_t pos = 5;

    Element value;

    ParsingStrategyFactory parsingStrategyFactory;
    ElementParser elementParser(parsingStrategyFactory.GetStrategy(ParsingType::ValueElement));

    elementParser.Parse(content, pos, value);

    CPPUNIT_ASSERT(value.GetName().empty());
    CPPUNIT_ASSERT(value.GetValue() == "Test");
}

void ElementParserTest::TestParseSingleTagElement()
{
    std::string content("<div>Test</div><br><div>Test</div>");
    size_t pos = 15;

    Element singleTagElement;

    ParsingStrategyFactory parsingStrategyFactory;
    ElementParser elementParser(parsingStrategyFactory.GetStrategy(ParsingType::SingleTagElement));

    elementParser.Parse(content, pos, singleTagElement);

    CPPUNIT_ASSERT(singleTagElement.GetName() == "br");
    CPPUNIT_ASSERT(singleTagElement.GetValue().empty());
}

void ElementParserTest::TestParseDefaultElement()
{
    std::string content("<h1 class=\"headers\" disabled>Test</h1>");
    size_t pos = 0;

    Element element;

    ParsingStrategyFactory parsingStrategyFactory;
    ElementParser elementParser(parsingStrategyFactory.GetStrategy(ParsingType::DefaultElement));

    elementParser.Parse(content, pos, element);

    CPPUNIT_ASSERT(element.GetName() == "h1");
    CPPUNIT_ASSERT(element.GetValue().empty());

    CPPUNIT_ASSERT(element.GetAttributes().size() == 2);

    CPPUNIT_ASSERT(element.GetAttributes().at(0).GetName() == "class");
    CPPUNIT_ASSERT(element.GetAttributes().at(0).GetValue() == "headers");

    CPPUNIT_ASSERT(element.GetAttributes().at(1).GetName() == "disabled");
    CPPUNIT_ASSERT(element.GetAttributes().at(1).GetValue().empty());
}