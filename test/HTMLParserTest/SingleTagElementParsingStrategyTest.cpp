#include "stdafx.h"
#include "SingleTagElementParsingStrategyTest.h"
#include "SingleTagElementParsingStrategy.h"

using namespace HTMLParserLibrary;

void SingleTagElementParsingStrategyTest::TestParse()
{
    std::string content("<div>Test</div><br><div>Test</div>");
    size_t pos = 15;

    Element singleTagElement;
    SingleTagElementParsingStrategy strategy;
    strategy.Parse(content, pos, singleTagElement);

    CPPUNIT_ASSERT(singleTagElement.GetName() == "br");
    CPPUNIT_ASSERT(singleTagElement.GetValue().empty());
}

void SingleTagElementParsingStrategyTest::TestParseWithAttributes()
{
    std::string content("<br id=\"first-br\" disabled>");
    size_t pos = 0;

    Element element;
    SingleTagElementParsingStrategy strategy;
    strategy.Parse(content, pos, element);

    CPPUNIT_ASSERT(element.GetName() == "br");
    CPPUNIT_ASSERT(element.GetValue().empty());

    CPPUNIT_ASSERT(element.GetAttributes().size() == 2);

    CPPUNIT_ASSERT(element.GetAttributes().at(0).GetName() == "id");
    CPPUNIT_ASSERT(element.GetAttributes().at(0).GetValue() == "first-br");

    CPPUNIT_ASSERT(element.GetAttributes().at(1).GetName() == "disabled");
    CPPUNIT_ASSERT(element.GetAttributes().at(1).GetValue().empty());
}

void SingleTagElementParsingStrategyTest::TestParseCloseTagElementWithAttributes()
{
    std::string content("</br id=\"first-br\" disabled>");
    size_t pos = 0;

    Element element;
    SingleTagElementParsingStrategy strategy;

    CPPUNIT_ASSERT_THROW(strategy.Parse(content, pos, element), std::exception);
}

void SingleTagElementParsingStrategyTest::TestParseSelfClosedTagElementWithAttributes()
{
    std::string content("<br id=\"first-br\" class=\"brs\"/>");
    size_t pos = 0;

    Element element;
    SingleTagElementParsingStrategy strategy;
    strategy.Parse(content, pos, element);

    CPPUNIT_ASSERT(element.GetName() == "br");
    CPPUNIT_ASSERT(element.GetValue().empty());

    CPPUNIT_ASSERT(element.GetAttributes().size() == 2);

    CPPUNIT_ASSERT(element.GetAttributes().at(0).GetName() == "id");
    CPPUNIT_ASSERT(element.GetAttributes().at(0).GetValue() == "first-br");

    CPPUNIT_ASSERT(element.GetAttributes().at(1).GetName() == "class");
    CPPUNIT_ASSERT(element.GetAttributes().at(1).GetValue() == "brs");
}

void SingleTagElementParsingStrategyTest::TestParseSelfClosedWhitespaceTagElementWithAttributes()
{
    std::string content("<br id=\"first-br\" class=\"brs\" />");
    size_t pos = 0;

    Element element;
    SingleTagElementParsingStrategy strategy;
    strategy.Parse(content, pos, element);

    CPPUNIT_ASSERT(element.GetName() == "br");
    CPPUNIT_ASSERT(element.GetValue().empty());

    CPPUNIT_ASSERT(element.GetAttributes().size() == 2);

    CPPUNIT_ASSERT(element.GetAttributes().at(0).GetName() == "id");
    CPPUNIT_ASSERT(element.GetAttributes().at(0).GetValue() == "first-br");

    CPPUNIT_ASSERT(element.GetAttributes().at(1).GetName() == "class");
    CPPUNIT_ASSERT(element.GetAttributes().at(1).GetValue() == "brs");
}

void SingleTagElementParsingStrategyTest::TestParseInvalidElement()
{
    std::string content("<br/");
    size_t pos = 0;

    Element element;
    SingleTagElementParsingStrategy strategy;
    CPPUNIT_ASSERT_THROW(strategy.Parse(content, pos, element), HTMLParsingException);
}

void SingleTagElementParsingStrategyTest::TestParseDoctypeHTML4()
{
    std::string content("<!DOCTYPE HTML PUBLIC \" -//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
    size_t pos = 0;

    Element element;
    SingleTagElementParsingStrategy strategy;
    strategy.Parse(content, pos, element);

    CPPUNIT_ASSERT(element.GetName() == "!DOCTYPE");
}

void SingleTagElementParsingStrategyTest::TestParseDoctypeHTML5()
{
    std::string content("<!DOCTYPE html>");
    size_t pos = 0;

    Element element;
    SingleTagElementParsingStrategy strategy;
    strategy.Parse(content, pos, element);

    CPPUNIT_ASSERT(element.GetName() == "!DOCTYPE");
}