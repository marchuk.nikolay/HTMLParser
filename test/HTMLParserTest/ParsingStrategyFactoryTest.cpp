#include "stdafx.h"
#include "ParsingStrategyFactoryTest.h"
#include "ParsingStrategyFactory.h"
#include "ValueElementParsingStrategy.h"
#include "SingleTagElementParsingStrategy.h"
#include "DefaultElementParsingStrategy.h"

using namespace HTMLParserLibrary;

void ParsingStrategyFactoryTest::TestGetValueElementStrategy()
{
    ParsingStrategyFactory parsingStrategyFactory;
    auto parsingStrategy = parsingStrategyFactory.GetStrategy(ParsingType::ValueElement);

    CPPUNIT_ASSERT(dynamic_cast<ValueElementParsingStrategy*>(parsingStrategy.get()) != NULL);
}

void ParsingStrategyFactoryTest::TestGetSingleTagElementStrategy()
{
    ParsingStrategyFactory parsingStrategyFactory;
    auto parsingStrategy = parsingStrategyFactory.GetStrategy(ParsingType::SingleTagElement);

    CPPUNIT_ASSERT(dynamic_cast<SingleTagElementParsingStrategy*>(parsingStrategy.get()) != NULL);
}

void ParsingStrategyFactoryTest::TestGetDefaultElementStrategy()
{
    ParsingStrategyFactory parsingStrategyFactory;
    auto parsingStrategy = parsingStrategyFactory.GetStrategy(ParsingType::DefaultElement);

    CPPUNIT_ASSERT(dynamic_cast<DefaultElementParsingStrategy*>(parsingStrategy.get()) != NULL);
}

void ParsingStrategyFactoryTest::TestUnknwonStrategy()
{
    ParsingStrategyFactory parsingStrategyFactory;

    CPPUNIT_ASSERT_THROW(parsingStrategyFactory.GetStrategy(static_cast<ParsingType>(100)), HTMLParsingException);
}