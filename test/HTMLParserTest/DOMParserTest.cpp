#include "stdafx.h"
#include "DOMParserTest.h"
#include "DOMParser.h"

using namespace HTMLParserLibrary;

void DOMParserTest::TestParseStringWithoutHTML()
{
    std::string content = "Text document";

    DOMParser parser;
    std::vector<Element> elements;
    CPPUNIT_ASSERT_THROW(parser.ParseElements(content, elements), HTMLParsingException);
}

void DOMParserTest::TestParseSingleElement()
{
    std::string content = "<h1></h1>";

    DOMParser parser;
    std::vector<Element> elements;
    parser.ParseElements(content, elements);

    CPPUNIT_ASSERT(elements.size() == 1);

    CPPUNIT_ASSERT(elements.at(0).GetName() == "h1");
    CPPUNIT_ASSERT(elements.at(0).GetValue().empty());
}

void DOMParserTest::TestParseHTMLWithoutNestingAndAttributes()
{
    std::string content = "<h1></h1><p></p><p></p>";

    DOMParser parser;
    std::vector<Element> elements;
    parser.ParseElements(content, elements);

    CPPUNIT_ASSERT(elements.size() == 3);
    CPPUNIT_ASSERT(elements.at(0).GetName() == "h1");
    CPPUNIT_ASSERT(elements.at(0).GetValue().empty());

    CPPUNIT_ASSERT(elements.at(1).GetName() == "p");
    CPPUNIT_ASSERT(elements.at(1).GetValue().empty());

    CPPUNIT_ASSERT(elements.at(2).GetName() == "p");
    CPPUNIT_ASSERT(elements.at(2).GetValue().empty());
}

void DOMParserTest::TestParseHTMLWithAttributes()
{
    std::string content = R"(<h1></h1><p class="paragraphs"></p><p class="paragraphs" disabled></p>)";

    DOMParser parser;
    std::vector<Element> elements;
    parser.ParseElements(content, elements);

    CPPUNIT_ASSERT(elements.size() == 3);
    CPPUNIT_ASSERT(elements.at(0).GetName() == "h1");
    CPPUNIT_ASSERT(elements.at(0).GetValue().empty());

    auto attributes = elements.at(0).GetAttributes();
    CPPUNIT_ASSERT(attributes.empty());

    CPPUNIT_ASSERT(elements.at(1).GetName() == "p");
    CPPUNIT_ASSERT(elements.at(1).GetValue().empty());

    attributes = elements.at(1).GetAttributes();
    CPPUNIT_ASSERT(attributes.size() == 1);
    CPPUNIT_ASSERT(attributes.at(0).GetName() == "class");
    CPPUNIT_ASSERT(attributes.at(0).GetValue() == "paragraphs");

    CPPUNIT_ASSERT(elements.at(2).GetName() == "p");
    CPPUNIT_ASSERT(elements.at(2).GetValue().empty());

    attributes = elements.at(2).GetAttributes();
    CPPUNIT_ASSERT(attributes.size() == 2);
    CPPUNIT_ASSERT(attributes.at(0).GetName() == "class");
    CPPUNIT_ASSERT(attributes.at(0).GetValue() == "paragraphs");
    CPPUNIT_ASSERT(attributes.at(1).GetName() == "disabled");
    CPPUNIT_ASSERT(attributes.at(1).GetValue().empty());
}

void DOMParserTest::TestParseNestedElement()
{
    std::string content = "<div><h1></h1></div>";

    DOMParser parser;
    std::vector<Element> elements;
    parser.ParseElements(content, elements);

    CPPUNIT_ASSERT(elements.at(0).GetName() == "div");
    CPPUNIT_ASSERT(elements.at(0).GetValue().empty());

    auto children = elements.at(0).GetChildren();
    CPPUNIT_ASSERT(children.size() == 1);

    CPPUNIT_ASSERT(children.at(0).GetName() == "h1");
    CPPUNIT_ASSERT(children.at(0).GetValue().empty());
}

void DOMParserTest::TestParseElementWithValue()
{
    std::string content = "<h1>Test header</h1>";

    DOMParser parser;
    std::vector<Element> elements;
    parser.ParseElements(content, elements);

    CPPUNIT_ASSERT(elements.at(0).GetName() == "h1");
    CPPUNIT_ASSERT(elements.at(0).GetValue().empty());

    auto children = elements.at(0).GetChildren();
    CPPUNIT_ASSERT(children.size() == 1);

    CPPUNIT_ASSERT(children.at(0).GetName().empty());
    CPPUNIT_ASSERT(children.at(0).GetValue() == "Test header");
}

void DOMParserTest::TestParseElementWithFewValues()
{
    std::string content = "<div>My <h1>Test</h1> header</div>";

    DOMParser parser;
    std::vector<Element> elements;
    parser.ParseElements(content, elements);

    CPPUNIT_ASSERT(elements.at(0).GetName() == "div");
    CPPUNIT_ASSERT(elements.at(0).GetValue().empty());

    auto children = elements.at(0).GetChildren();
    CPPUNIT_ASSERT(children.size() == 3);

    CPPUNIT_ASSERT(children.at(0).GetName().empty());
    CPPUNIT_ASSERT(children.at(0).GetValue() == "My ");

    CPPUNIT_ASSERT(children.at(1).GetName() == "h1");
    CPPUNIT_ASSERT(children.at(1).GetValue().empty());

    CPPUNIT_ASSERT(children.at(2).GetName().empty());
    CPPUNIT_ASSERT(children.at(2).GetValue() == " header");

    auto headerChildren = children.at(1).GetChildren();
    CPPUNIT_ASSERT(headerChildren.size() == 1);

    CPPUNIT_ASSERT(headerChildren.at(0).GetName().empty());
    CPPUNIT_ASSERT(headerChildren.at(0).GetValue() == "Test");
}

void DOMParserTest::TestParseNestedElementsWithAttributes()
{
    std::string content = R"(<div><h1 class="headers">First Header</h1></div>)";

    DOMParser parser;
    std::vector<Element> elements;
    parser.ParseElements(content, elements);

    CPPUNIT_ASSERT(elements.size() == 1);

    CPPUNIT_ASSERT(elements.at(0).GetName() == "div");
    CPPUNIT_ASSERT(elements.at(0).GetValue().empty());

    auto divAttributes = elements.at(0).GetAttributes();
    CPPUNIT_ASSERT(divAttributes.empty());

    auto divChildren = elements.at(0).GetChildren();
    CPPUNIT_ASSERT(divChildren.size() == 1);

    CPPUNIT_ASSERT(divChildren.at(0).GetName() == "h1");
    CPPUNIT_ASSERT(divChildren.at(0).GetValue().empty());

    auto headerAttributes = divChildren.at(0).GetAttributes();
    CPPUNIT_ASSERT(headerAttributes.size() == 1);

    CPPUNIT_ASSERT(headerAttributes.at(0).GetName() == "class");
    CPPUNIT_ASSERT(headerAttributes.at(0).GetValue() == "headers");

    auto headerChildren = divChildren.at(0).GetChildren();
    CPPUNIT_ASSERT(headerChildren.size() == 1);

    CPPUNIT_ASSERT(headerChildren.at(0).GetName().empty());
    CPPUNIT_ASSERT(headerChildren.at(0).GetValue() == "First Header");
}

void DOMParserTest::TestSkipStyleElements()
{
    std::string content = "<style>p {color: blue;}</style><h1>Test header</h1><p>Test paragraph</p>";

    DOMParser parser;
    std::vector<Element> elements;
    parser.ParseElements(content, elements);

    CPPUNIT_ASSERT(elements.size() == 2);

    CPPUNIT_ASSERT(elements.at(0).GetName() == "h1");
    CPPUNIT_ASSERT(elements.at(0).GetValue().empty());

    CPPUNIT_ASSERT(elements.at(1).GetName() == "p");
    CPPUNIT_ASSERT(elements.at(1).GetValue().empty());

    auto headerChildren = elements.at(0).GetChildren();
    CPPUNIT_ASSERT(headerChildren.size() == 1);

    CPPUNIT_ASSERT(headerChildren.at(0).GetName().empty());
    CPPUNIT_ASSERT(headerChildren.at(0).GetValue() == "Test header");

    auto paragraphChildren = elements.at(1).GetChildren();
    CPPUNIT_ASSERT(paragraphChildren.size() == 1);

    CPPUNIT_ASSERT(paragraphChildren.at(0).GetName().empty());
    CPPUNIT_ASSERT(paragraphChildren.at(0).GetValue() == "Test paragraph");
}

void DOMParserTest::TestOnlyOpenStyleTag()
{
    std::string content = "<style>p {color: blue;}<h1>Test header</h1><p>Test paragraph</p>";

    DOMParser parser;
    std::vector<Element> elements;

    CPPUNIT_ASSERT_THROW(parser.ParseElements(content, elements), HTMLParsingException);
}

void DOMParserTest::TestOnlyCloseStyleTag()
{
    std::string content = "p {color: blue;}</style><h1>Test header</h1><p>Test paragraph</p>";

    DOMParser parser;
    std::vector<Element> elements;

    CPPUNIT_ASSERT_THROW(parser.ParseElements(content, elements), HTMLParsingException);
}

void DOMParserTest::TestSkipScriptElements()
{
    std::string content = "<script>console.log('Message');</script><h1>Test header</h1><p>Test paragraph</p>";

    DOMParser parser;
    std::vector<Element> elements;
    parser.ParseElements(content, elements);

    CPPUNIT_ASSERT(elements.size() == 2);

    CPPUNIT_ASSERT(elements.at(0).GetName() == "h1");
    CPPUNIT_ASSERT(elements.at(0).GetValue().empty());

    CPPUNIT_ASSERT(elements.at(1).GetName() == "p");
    CPPUNIT_ASSERT(elements.at(1).GetValue().empty());

    auto headerChildren = elements.at(0).GetChildren();
    CPPUNIT_ASSERT(headerChildren.size() == 1);

    CPPUNIT_ASSERT(headerChildren.at(0).GetName().empty());
    CPPUNIT_ASSERT(headerChildren.at(0).GetValue() == "Test header");

    auto paragraphChildren = elements.at(1).GetChildren();
    CPPUNIT_ASSERT(paragraphChildren.size() == 1);

    CPPUNIT_ASSERT(paragraphChildren.at(0).GetName().empty());
    CPPUNIT_ASSERT(paragraphChildren.at(0).GetValue() == "Test paragraph");
}

void DOMParserTest::TestSkipComments()
{
    std::string content = "<!-- Header --><h1>Test header</h1><!-- Header -->";

    DOMParser parser;
    std::vector<Element> elements;
    parser.ParseElements(content, elements);

    CPPUNIT_ASSERT(elements.size() == 1);

    CPPUNIT_ASSERT(elements.at(0).GetName() == "h1");
    CPPUNIT_ASSERT(elements.at(0).GetValue().empty());

    auto children = elements.at(0).GetChildren();
    CPPUNIT_ASSERT(children.size() == 1);

    CPPUNIT_ASSERT(children.at(0).GetName().empty());
    CPPUNIT_ASSERT(children.at(0).GetValue() == "Test header");
}

void DOMParserTest::TestSkipUnnecessaryElements()
{
    std::string content = "<style>p {color: blue;}</style><script>console.log('Message');</script><!-- Header --><h1>Test header</h1><p>Test paragraph</p>";

    DOMParser parser;
    std::vector<Element> elements;
    parser.ParseElements(content, elements);

    CPPUNIT_ASSERT(elements.size() == 2);

    CPPUNIT_ASSERT(elements.at(0).GetName() == "h1");
    CPPUNIT_ASSERT(elements.at(0).GetValue().empty());

    CPPUNIT_ASSERT(elements.at(1).GetName() == "p");
    CPPUNIT_ASSERT(elements.at(1).GetValue().empty());

    auto headerChildren = elements.at(0).GetChildren();
    CPPUNIT_ASSERT(headerChildren.size() == 1);

    CPPUNIT_ASSERT(headerChildren.at(0).GetName().empty());
    CPPUNIT_ASSERT(headerChildren.at(0).GetValue() == "Test header");

    auto paragraphChildren = elements.at(1).GetChildren();
    CPPUNIT_ASSERT(paragraphChildren.size() == 1);

    CPPUNIT_ASSERT(paragraphChildren.at(0).GetName().empty());
    CPPUNIT_ASSERT(paragraphChildren.at(0).GetValue() == "Test paragraph");
}

void DOMParserTest::TestStringWithSkippedElements()
{
    std::string content = "<style>p {color: blue;}</style><script>console.log('Message');</script>";

    DOMParser parser;
    std::vector<Element> elements;
    CPPUNIT_ASSERT_THROW(parser.ParseElements(content, elements), HTMLParsingException);
}

void DOMParserTest::TestParseSingleTagElements()
{
    std::string content = "<h1>Test header</h1><br><p>Test paragraph</p><br /><p>Test paragraph</p></br><p>Test paragraph</p><br/>";

    DOMParser parser;
    std::vector<Element> elements;
    parser.ParseElements(content, elements);

    CPPUNIT_ASSERT(elements.size() == 8);

    CPPUNIT_ASSERT(elements.at(0).GetName() == "h1");
    CPPUNIT_ASSERT(elements.at(1).GetName() == "br");
    CPPUNIT_ASSERT(elements.at(2).GetName() == "p");
    CPPUNIT_ASSERT(elements.at(3).GetName() == "br");
    CPPUNIT_ASSERT(elements.at(4).GetName() == "p");
    CPPUNIT_ASSERT(elements.at(5).GetName() == "br");
    CPPUNIT_ASSERT(elements.at(6).GetName() == "p");
    CPPUNIT_ASSERT(elements.at(7).GetName() == "br");
}

void DOMParserTest::TestParseEmptyString()
{
    std::string content = "";

    DOMParser parser;
    std::vector<Element> elements;
    parser.ParseElements(content, elements);

    CPPUNIT_ASSERT(elements.empty());
}

void DOMParserTest::TestParseStringWithTabsAndNewLines()
{
    std::string content = "<div>\n\t<h1></h1></div>";

    DOMParser parser;
    std::vector<Element> elements;
    parser.ParseElements(content, elements);

    CPPUNIT_ASSERT(elements.size() == 1);

    CPPUNIT_ASSERT(elements.at(0).GetName() == "div");
    CPPUNIT_ASSERT(elements.at(0).GetValue().empty());

    auto divChildren = elements.at(0).GetChildren();
    CPPUNIT_ASSERT(divChildren.size() == 1);

    CPPUNIT_ASSERT(divChildren.at(0).GetName() == "h1");
    CPPUNIT_ASSERT(divChildren.at(0).GetValue().empty());
}

void DOMParserTest::TestParseElementWithOnlyOpenTag()
{
    std::string content = "<div>Test div";

    DOMParser parser;
    std::vector<Element> elements;
    CPPUNIT_ASSERT_THROW(parser.ParseElements(content, elements), HTMLParsingException);
}

void DOMParserTest::TestParseElementWithOnlyCloseTag()
{
    std::string content = "Test div</div>";

    DOMParser parser;
    std::vector<Element> elements;
    CPPUNIT_ASSERT_THROW(parser.ParseElements(content, elements), HTMLParsingException);
}

void DOMParserTest::TestParseElementWithDifferentOpenAndCloseTags()
{
    std::string content = "<h1>Test div</div>";

    DOMParser parser;
    std::vector<Element> elements;
    CPPUNIT_ASSERT_THROW(parser.ParseElements(content, elements), HTMLParsingException);
}

void DOMParserTest::TestParseCloseTagBeforeOpen()
{
    std::string content = "</div>Test div<div>";

    DOMParser parser;
    std::vector<Element> elements;
    CPPUNIT_ASSERT_THROW(parser.ParseElements(content, elements), HTMLParsingException);
}

void DOMParserTest::TestParseDifferentNumberOfOpenAndCloseTags()
{
    std::string content = "<div>Test div 1<div> Test div 2<div>";

    DOMParser parser;
    std::vector<Element> elements;
    CPPUNIT_ASSERT_THROW(parser.ParseElements(content, elements), HTMLParsingException);
}

void DOMParserTest::TestParseElementWithIncorrectCloseTag()
{
    std::string content = "<div>Test</br";

    DOMParser parser;
    std::vector<Element> elements;
    CPPUNIT_ASSERT_THROW(parser.ParseElements(content, elements), HTMLParsingException);
}