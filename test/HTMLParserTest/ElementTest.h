#pragma once

#include "cppunit\TestFixture.h"
#include "cppunit\extensions\HelperMacros.h"

class ElementTest : public CppUnit::TestFixture
{
public:
    void TestGetName();
    void TestSetName();
    void TestSetGetValue();
    void TestAddAttribute();
    void TestAddAttributeWithoutValue();
    void TestRemoveAttribute();
    void TestRemoveAttributeWithoutValue();
    void TestRemoveAttributeThatDoesntExist();
    void TestAddAttributes();
    void TestAddEmptyCollectionOfAttributes();
    void TestRemoveAttributes();
    void TestRemoveZeroAttributes();
    void TestAddToBegin();
    void TestAddToEnd();
    void TestAddChildren();
    void TestAddEmptyCollectionOfChildren();
    void TestSearchOneResult();
    void TestSearchNoElementsFound();
    void TestSearchFewResults();
    void TestSearchFewResultsOnDifferentLevels();
    void TestSearchEmptySelector();
    void TestSearchComplexSelector();
    void TestSearchComplexSelectorFewResults();
    void TestSearchUsingClass();
    void TestSearchUsingId();
    void TestSearchAttribute();
    void TestSearchAttributeWithoutValue();
    void TestSearchAttributeNoResults();
    void TestSearchComplexSelectorWithAttribute();
    void TestHasAttribute();
    void TestHasAttributeWithoutValue();
    void TestHasAttributeFalseResult();
    void TestIsElementSatisfyingTagSelector();
    void TestIsElementSatisfyingClassSelector();
    void TestIsElementSatisfyingIdSelector();
    void TestIsElementSatisfyingAttributeSelector();
    void TestIsElementSatisfyingAttributeWithoutValueSelector();
    void TestIsElementSatisfyingIncorrectSelector();
    void TestGetHTML();
    void TestGetHTMLEmptyElement();
    void TestSetHTML();
    void TestSetEmptyHTML();
    void TestGetText();
    void TestGetTextEmptyHTML();
    void TestSetText();
    void TestSetTextEmptyString();

    CPPUNIT_TEST_SUITE(ElementTest);

    CPPUNIT_TEST(TestGetName);
    CPPUNIT_TEST(TestSetName);
    CPPUNIT_TEST(TestSetGetValue);
    CPPUNIT_TEST(TestAddAttribute);
    CPPUNIT_TEST(TestAddAttributeWithoutValue);
    CPPUNIT_TEST(TestRemoveAttribute);
    CPPUNIT_TEST(TestRemoveAttributeWithoutValue);
    CPPUNIT_TEST(TestRemoveAttributeThatDoesntExist);
    CPPUNIT_TEST(TestAddAttributes);
    CPPUNIT_TEST(TestAddEmptyCollectionOfAttributes);
    CPPUNIT_TEST(TestRemoveAttributes);
    CPPUNIT_TEST(TestRemoveZeroAttributes);
    CPPUNIT_TEST(TestAddToBegin);
    CPPUNIT_TEST(TestAddToEnd);
    CPPUNIT_TEST(TestAddChildren);
    CPPUNIT_TEST(TestAddEmptyCollectionOfChildren);
    CPPUNIT_TEST(TestSearchOneResult);
    CPPUNIT_TEST(TestSearchNoElementsFound);
    CPPUNIT_TEST(TestSearchFewResults);
    CPPUNIT_TEST(TestSearchFewResultsOnDifferentLevels);
    CPPUNIT_TEST(TestSearchEmptySelector);
    CPPUNIT_TEST(TestSearchComplexSelector);
    CPPUNIT_TEST(TestSearchComplexSelectorFewResults);
    CPPUNIT_TEST(TestSearchUsingClass);
    CPPUNIT_TEST(TestSearchUsingId);
    CPPUNIT_TEST(TestSearchAttribute);
    CPPUNIT_TEST(TestSearchAttributeWithoutValue);
    CPPUNIT_TEST(TestSearchAttributeNoResults);
    CPPUNIT_TEST(TestSearchComplexSelectorWithAttribute);
    CPPUNIT_TEST(TestHasAttribute);
    CPPUNIT_TEST(TestHasAttributeWithoutValue);
    CPPUNIT_TEST(TestHasAttributeFalseResult);
    CPPUNIT_TEST(TestIsElementSatisfyingTagSelector);
    CPPUNIT_TEST(TestIsElementSatisfyingClassSelector);
    CPPUNIT_TEST(TestIsElementSatisfyingIdSelector);
    CPPUNIT_TEST(TestIsElementSatisfyingAttributeSelector);
    CPPUNIT_TEST(TestIsElementSatisfyingAttributeWithoutValueSelector);
    CPPUNIT_TEST(TestIsElementSatisfyingIncorrectSelector);
    CPPUNIT_TEST(TestGetHTML);
    CPPUNIT_TEST(TestGetHTMLEmptyElement);
    CPPUNIT_TEST(TestSetHTML);
    CPPUNIT_TEST(TestSetEmptyHTML);
    CPPUNIT_TEST(TestGetText);
    CPPUNIT_TEST(TestGetTextEmptyHTML);
    CPPUNIT_TEST(TestSetText);
    CPPUNIT_TEST(TestSetTextEmptyString);

    CPPUNIT_TEST_SUITE_END();
};