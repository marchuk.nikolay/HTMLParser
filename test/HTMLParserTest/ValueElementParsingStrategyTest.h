#pragma once

#include "cppunit\TestFixture.h"
#include "cppunit\extensions\HelperMacros.h"

class ValueElementParsingStrategyTest : public CppUnit::TestFixture
{
public:
    void TestParse();
    void TestParseIncorrectDocument();

    CPPUNIT_TEST_SUITE(ValueElementParsingStrategyTest);

    CPPUNIT_TEST(TestParse);
    CPPUNIT_TEST(TestParseIncorrectDocument);

    CPPUNIT_TEST_SUITE_END();
};