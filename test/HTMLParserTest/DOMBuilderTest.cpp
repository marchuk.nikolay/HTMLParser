#include "stdafx.h"
#include "DOMBuilderTest.h"
#include "DOMBuilder.h"

using namespace HTMLParserLibrary;

void DOMBuilderTest::TestInsertElement()
{
    Element element("div");

    DOMBuilder domBuilder;
    domBuilder.InsertElement(element, 1);

    auto dom = domBuilder.GetDOM();
    CPPUNIT_ASSERT(dom.size() == 1);

    CPPUNIT_ASSERT(dom.at(0).GetName() == "div");
    CPPUNIT_ASSERT(dom.at(0).GetValue().empty());
}

void DOMBuilderTest::TestInsertElementInIncorrectPlace()
{
    Element element("div");

    DOMBuilder domBuilder;
    CPPUNIT_ASSERT_THROW(domBuilder.InsertElement(element, 3), HTMLParsingException);
}

void DOMBuilderTest::TestInsertElementNesting()
{
    Element div("div");

    DOMBuilder domBuilder;
    domBuilder.InsertElement(div, 1);

    Element header("h1");
    domBuilder.InsertElement(header, 2);

    auto dom = domBuilder.GetDOM();
    CPPUNIT_ASSERT(dom.size() == 1);

    CPPUNIT_ASSERT(dom.at(0).GetName() == "div");
    CPPUNIT_ASSERT(dom.at(0).GetValue().empty());

    CPPUNIT_ASSERT(dom.at(0).GetChildren().size() == 1);

    CPPUNIT_ASSERT(dom.at(0).GetChildren().at(0).GetName() == "h1");
    CPPUNIT_ASSERT(dom.at(0).GetChildren().at(0).GetValue().empty());
}

void DOMBuilderTest::TestGetDOM()
{
    DOMBuilder domBuilder;

    auto dom = domBuilder.GetDOM();
    CPPUNIT_ASSERT(dom.empty());
}