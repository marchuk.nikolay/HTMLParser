#include "stdafx.h"
#include "ElementTest.h"
#include "Element.h"
#include "Attribute.h"

using namespace HTMLParserLibrary;

void ElementTest::TestGetName()
{
    Element element("p");
    CPPUNIT_ASSERT(element.GetName() == "p");
}

void ElementTest::TestSetName()
{
    Element element("h1");
    element.SetName("h2");

    CPPUNIT_ASSERT(element.GetName() == "h2");
}

void ElementTest::TestSetGetValue()
{
    Element element;

    element.SetValue("Paragraph");
    CPPUNIT_ASSERT(element.GetValue() == "Paragraph");
}

void ElementTest::TestAddAttribute()
{
    Element element("h1");
    Attribute attribute("id", "first-header");

    element.AddAttribute(attribute);
    auto elementAttributes = element.GetAttributes();

    CPPUNIT_ASSERT(elementAttributes.size() == 1);

    CPPUNIT_ASSERT(elementAttributes.at(0).GetName() == "id");
    CPPUNIT_ASSERT(elementAttributes.at(0).GetValue() == "first-header");
}

void ElementTest::TestAddAttributeWithoutValue()
{
    Element element("h1");
    Attribute attribute("disabled");

    element.AddAttribute(attribute);
    auto elementAttributes = element.GetAttributes();

    CPPUNIT_ASSERT(elementAttributes.size() == 1);

    CPPUNIT_ASSERT(elementAttributes.at(0).GetName() == "disabled");
    CPPUNIT_ASSERT(elementAttributes.at(0).GetValue().empty());
}

void ElementTest::TestRemoveAttribute()
{
    Element element("h1");
    Attribute attribute("id", "first-header");

    element.AddAttribute(attribute);
    auto elementAttributes = element.GetAttributes();

    CPPUNIT_ASSERT(elementAttributes.size() == 1);

    element.RemoveAttribute("id");
    CPPUNIT_ASSERT(element.GetAttributes().empty());
}

void ElementTest::TestRemoveAttributeWithoutValue()
{
    Element element("h1");
    Attribute attribute("disabled");

    element.AddAttribute(attribute);
    auto elementAttributes = element.GetAttributes();

    CPPUNIT_ASSERT(elementAttributes.size() == 1);

    element.RemoveAttribute("disabled");
    CPPUNIT_ASSERT(element.GetAttributes().empty());
}

void ElementTest::TestRemoveAttributeThatDoesntExist()
{
    Element element("h1");
    CPPUNIT_ASSERT_THROW(element.RemoveAttribute("class"), HTMLParsingException);
}

void ElementTest::TestAddAttributes()
{
    Element element("h1");
    Attribute attribute1("id", "first-header");
    Attribute attribute2("disabled");

    std::vector<Attribute> attributes;
    attributes.push_back(attribute1);
    attributes.push_back(attribute2);

    element.AddAttributes(attributes);
    auto elementAttributes = element.GetAttributes();

    CPPUNIT_ASSERT(elementAttributes.size() == 2);

    CPPUNIT_ASSERT(elementAttributes.at(0).GetName() == "id");
    CPPUNIT_ASSERT(elementAttributes.at(0).GetValue() == "first-header");

    CPPUNIT_ASSERT(elementAttributes.at(1).GetName() == "disabled");
    CPPUNIT_ASSERT(elementAttributes.at(1).GetValue().empty());
}

void ElementTest::TestAddEmptyCollectionOfAttributes()
{
    Element element("h1");

    std::vector<Attribute> attributes;

    CPPUNIT_ASSERT_NO_THROW(element.AddAttributes(attributes));
    auto elementAttributes = element.GetAttributes();

    CPPUNIT_ASSERT(elementAttributes.empty());
}

void ElementTest::TestRemoveAttributes()
{
    Element element("h1");
    Attribute attribute1("id", "first-header");
    Attribute attribute2("disabled");

    std::vector<Attribute> attributes;
    attributes.push_back(attribute1);
    attributes.push_back(attribute2);

    element.AddAttributes(attributes);
    auto elementAttributes = element.GetAttributes();

    CPPUNIT_ASSERT(elementAttributes.size() == 2);

    element.RemoveAttributes();
    CPPUNIT_ASSERT(element.GetAttributes().empty());
}

void ElementTest::TestRemoveZeroAttributes()
{
    Element element("h1");
    CPPUNIT_ASSERT_NO_THROW(element.RemoveAttributes());
}

void ElementTest::TestAddToEnd()
{
    Element element("div");
    Element nestedHeader("h1");
    Element nestedParagraph("p");

    element.AddToEnd(nestedHeader);
    element.AddToEnd(nestedParagraph);

    auto children = element.GetChildren();

    CPPUNIT_ASSERT(children.size() == 2);

    CPPUNIT_ASSERT(children.at(0).GetName() == "h1");
    CPPUNIT_ASSERT(children.at(1).GetName() == "p");
}

void ElementTest::TestAddToBegin()
{
    Element element("div");
    Element nestedHeader("h1");
    Element nestedParagraph("p");

    element.AddToBegin(nestedParagraph);
    element.AddToBegin(nestedHeader);

    auto children = element.GetChildren();

    CPPUNIT_ASSERT(children.size() == 2);

    CPPUNIT_ASSERT(children.at(0).GetName() == "h1");
    CPPUNIT_ASSERT(children.at(1).GetName() == "p");
}

void ElementTest::TestAddChildren()
{
    Element element("div");
    Element nestedHeader("h1");
    Element nestedParagraph("p");

    std::vector<Element> nestedElement;
    nestedElement.push_back(nestedHeader);
    nestedElement.push_back(nestedParagraph);

    element.AddChildren(nestedElement);

    auto children = element.GetChildren();

    CPPUNIT_ASSERT(children.size() == 2);

    CPPUNIT_ASSERT(children.at(0).GetName() == "h1");
    CPPUNIT_ASSERT(children.at(1).GetName() == "p");
}

void ElementTest::TestAddEmptyCollectionOfChildren()
{
    Element element("div");

    std::vector<Element> nestedElement;
    CPPUNIT_ASSERT_NO_THROW(element.AddChildren(nestedElement));

    auto children = element.GetChildren();
    CPPUNIT_ASSERT(children.empty());
}

void ElementTest::TestSearchOneResult()
{
    Element div("div");
    Element header("h1");

    div.AddToEnd(header);

    std::vector<Element> result;
    div.Search("h1", result);
    
    CPPUNIT_ASSERT(result.size() == 1);
    CPPUNIT_ASSERT(result.at(0).GetName() == "h1");
    CPPUNIT_ASSERT(result.at(0).GetValue().empty());
}

void ElementTest::TestSearchNoElementsFound()
{
    Element div("div");

    std::vector<Element> result;
    div.Search("h1", result);

    CPPUNIT_ASSERT(result.empty());
}

void ElementTest::TestSearchFewResults()
{
    Element div("div");
    Element firestHeader("h1");
    Element secondHeader("h1");

    div.AddToEnd(firestHeader);
    div.AddToEnd(secondHeader);

    std::vector<Element> result;
    div.Search("h1", result);

    CPPUNIT_ASSERT(result.size() == 2);

    CPPUNIT_ASSERT(result.at(0).GetName() == "h1");
    CPPUNIT_ASSERT(result.at(0).GetValue().empty());

    CPPUNIT_ASSERT(result.at(1).GetName() == "h1");
    CPPUNIT_ASSERT(result.at(1).GetValue().empty());
}

void ElementTest::TestSearchFewResultsOnDifferentLevels()
{
    Element nestedHeader("h1");
    Element nestedDiv("div");
    nestedDiv.AddToEnd(nestedHeader);

    Element header("h1");

    Element div("div");
    div.AddToEnd(header);
    div.AddToEnd(nestedDiv);

    std::vector<Element> result;
    div.Search("h1", result);

    CPPUNIT_ASSERT(result.size() == 2);

    CPPUNIT_ASSERT(result.at(0).GetName() == "h1");
    CPPUNIT_ASSERT(result.at(0).GetValue().empty());

    CPPUNIT_ASSERT(result.at(1).GetName() == "h1");
    CPPUNIT_ASSERT(result.at(1).GetValue().empty());
}

void ElementTest::TestSearchEmptySelector()
{
    Element div("div");
    Element header("h1");

    div.AddToEnd(header);

    std::vector<Element> result;
    div.Search("", result);

    CPPUNIT_ASSERT(result.empty());
}

void ElementTest::TestSearchComplexSelector()
{
    Element header("h1");
    Element nestedDiv("div");
    nestedDiv.AddToEnd(header);

    Element div("div");
    div.AddToEnd(nestedDiv);

    std::vector<Element> result;
    div.Search("div h1", result);

    CPPUNIT_ASSERT(result.size() == 1);
    CPPUNIT_ASSERT(result.at(0).GetName() == "h1");
    CPPUNIT_ASSERT(result.at(0).GetValue().empty());
}

void ElementTest::TestSearchComplexSelectorFewResults()
{
    Element firstHeaderText;
    firstHeaderText.SetValue("First");

    Element firstHeader("h1");
    firstHeader.AddToEnd(firstHeaderText);

    Element firstNestedDiv("div");
    firstNestedDiv.AddToEnd(firstHeader);

    Element secondHeaderText;
    secondHeaderText.SetValue("Second");

    Element secondHeader("h1");
    secondHeader.AddToEnd(secondHeaderText);

    Element secondNestedDiv("div");
    secondNestedDiv.AddToEnd(secondHeader);

    Element header("h1");

    Element div("div");
    div.AddToEnd(header);
    div.AddToEnd(firstNestedDiv);
    div.AddToEnd(secondNestedDiv);

    std::vector<Element> result;
    div.Search("div h1", result);

    CPPUNIT_ASSERT(result.size() == 2);
    CPPUNIT_ASSERT(result.at(0).GetName() == "h1");
    CPPUNIT_ASSERT(result.at(0).GetChildren().at(0).GetValue() == "First");

    CPPUNIT_ASSERT(result.at(1).GetName() == "h1");
    CPPUNIT_ASSERT(result.at(1).GetChildren().at(0).GetValue() == "Second");
}

void ElementTest::TestSearchUsingClass()
{
    Element div("div");
    Element header("h1");

    Attribute attribute("class", "headers");
    header.AddAttribute(attribute);

    div.AddToEnd(header);

    std::vector<Element> result;
    div.Search(".headers", result);

    CPPUNIT_ASSERT(result.size() == 1);
    CPPUNIT_ASSERT(result.at(0).GetName() == "h1");
    CPPUNIT_ASSERT(result.at(0).GetValue().empty());
}

void ElementTest::TestSearchUsingId()
{
    Element div("div");
    Element header("h1");

    Attribute attribute("id", "header");
    header.AddAttribute(attribute);

    div.AddToEnd(header);

    std::vector<Element> result;
    div.Search("#header", result);

    CPPUNIT_ASSERT(result.size() == 1);
    CPPUNIT_ASSERT(result.at(0).GetName() == "h1");
    CPPUNIT_ASSERT(result.at(0).GetValue().empty());
}

void ElementTest::TestSearchAttribute()
{
    Element div("div");
    Element header("h1");

    Attribute attribute("title", "header");
    header.AddAttribute(attribute);

    div.AddToEnd(header);

    std::vector<Element> result;
    div.Search("[title=header]", result);

    CPPUNIT_ASSERT(result.size() == 1);
    CPPUNIT_ASSERT(result.at(0).GetName() == "h1");
    CPPUNIT_ASSERT(result.at(0).GetValue().empty());
}

void ElementTest::TestSearchAttributeWithoutValue()
{
    Element div("div");
    Element header("h1");

    Attribute attribute("disabled");
    header.AddAttribute(attribute);

    div.AddToEnd(header);

    std::vector<Element> result;
    div.Search("[disabled]", result);

    CPPUNIT_ASSERT(result.size() == 1);
    CPPUNIT_ASSERT(result.at(0).GetName() == "h1");
    CPPUNIT_ASSERT(result.at(0).GetValue().empty());
}

void ElementTest::TestSearchAttributeNoResults()
{
    Element div("div");
    Element header("h1");

    Attribute attribute("disabled");
    header.AddAttribute(attribute);

    div.AddToEnd(header);

    std::vector<Element> result;
    div.Search("[title=header]", result);

    CPPUNIT_ASSERT(result.empty());
}

void ElementTest::TestSearchComplexSelectorWithAttribute()
{
    Element header("h1");

    Attribute attribute("class", "headers");
    header.AddAttribute(attribute);

    Element nestedDiv("div");
    nestedDiv.AddToEnd(header);

    Element div("div");
    div.AddToEnd(nestedDiv);

    std::vector<Element> result;
    div.Search("div .headers", result);

    CPPUNIT_ASSERT(result.size() == 1);

    CPPUNIT_ASSERT(result.at(0).GetName() == "h1");
    CPPUNIT_ASSERT(result.at(0).GetValue().empty());
}

void ElementTest::TestHasAttribute()
{
    Element header("h1");

    Attribute attribute("class", "headers");
    header.AddAttribute(attribute);

    CPPUNIT_ASSERT(header.HasAttribute("class", "headers"));
}

void ElementTest::TestHasAttributeWithoutValue()
{
    Element header("h1");

    Attribute attribute("disabled");
    header.AddAttribute(attribute);

    CPPUNIT_ASSERT(header.HasAttribute("disabled", ""));
}

void ElementTest::TestHasAttributeFalseResult()
{
    Element header("h1");

    CPPUNIT_ASSERT(!header.HasAttribute("class", "headers"));
}

void ElementTest::TestIsElementSatisfyingTagSelector()
{
    Element header("h1");

    CPPUNIT_ASSERT(header.IsElementSatisfyingSelector("h1"));
}

void ElementTest::TestIsElementSatisfyingClassSelector()
{
    Element header("h1");
    Attribute attribute("class", "headers");

    header.AddAttribute(attribute);

    CPPUNIT_ASSERT(header.IsElementSatisfyingSelector(".headers"));
}

void ElementTest::TestIsElementSatisfyingIdSelector()
{
    Element header("h1");
    Attribute attribute("id", "header");

    header.AddAttribute(attribute);

    CPPUNIT_ASSERT(header.IsElementSatisfyingSelector("#header"));
}

void ElementTest::TestIsElementSatisfyingAttributeSelector()
{
    Element header("h1");
    Attribute attribute("title", "header");

    header.AddAttribute(attribute);

    CPPUNIT_ASSERT(header.IsElementSatisfyingSelector("[title=header]"));
}

void ElementTest::TestIsElementSatisfyingAttributeWithoutValueSelector()
{
    Element header("h1");
    Attribute attribute("disabled");

    header.AddAttribute(attribute);

    CPPUNIT_ASSERT(header.IsElementSatisfyingSelector("[disabled]"));
}

void ElementTest::TestIsElementSatisfyingIncorrectSelector()
{
    Element header("h1");

    CPPUNIT_ASSERT(!header.IsElementSatisfyingSelector("(disabled)"));
}

void ElementTest::TestGetHTML()
{
    Element headerText;
    headerText.SetValue("Test");

    Element header("h1");
    header.AddToEnd(headerText);

    Attribute id("id", "header");
    header.AddAttribute(id);

    Attribute disabled("disabled");
    header.AddAttribute(disabled);

    Element paragraphText;
    paragraphText.SetValue("paragraph");

    Element paragraph("p");
    paragraph.AddToEnd(paragraphText);

    Element div("div");
    div.AddToEnd(header);
    div.AddToEnd(paragraph);

    std::string html = "";
    div.GetHTML(html);

    CPPUNIT_ASSERT(html == "<div><h1 id=\"header\" disabled>Test</h1><p>paragraph</p></div>");
}

void ElementTest::TestGetHTMLEmptyElement()
{
    Element header;

    std::string html = "";
    header.GetHTML(html);

    CPPUNIT_ASSERT(html.empty());
}

void ElementTest::TestSetHTML()
{
    Element headerText;
    headerText.SetValue("header");

    Element header("h1");
    header.AddToEnd(headerText);

    Element div("div");
    div.AddToEnd(header);

    div.SetHTML("<p class=\"p\">paragraph</p>");

    auto& children = div.GetChildren();

    CPPUNIT_ASSERT(children.at(0).GetName() == "p");
    CPPUNIT_ASSERT(children.at(0).GetChildren().at(0).GetValue() == "paragraph");

    CPPUNIT_ASSERT(children.at(0).GetAttributes().at(0).GetName() == "class");
    CPPUNIT_ASSERT(children.at(0).GetAttributes().at(0).GetValue() == "p");
}

void ElementTest::TestSetEmptyHTML()
{
    Element headerText;
    headerText.SetValue("header");

    Element header("h1");
    header.AddToEnd(headerText);

    Element div("div");
    div.AddToEnd(header);

    div.SetHTML("");

    CPPUNIT_ASSERT(div.GetChildren().empty());
}

void ElementTest::TestGetText()
{
    Element headerText;
    headerText.SetValue("Test ");

    Element header("h1");
    header.AddToEnd(headerText);

    Attribute id("id", "header");
    header.AddAttribute(id);

    Attribute disabled("disabled");
    header.AddAttribute(disabled);

    Element paragraphText;
    paragraphText.SetValue("paragraph");

    Element paragraph("p");
    paragraph.AddToEnd(paragraphText);

    Element div("div");
    div.AddToEnd(header);
    div.AddToEnd(paragraph);

    std::string html = "";
    div.GetText(html);

    CPPUNIT_ASSERT(html == "Test paragraph");
}

void ElementTest::TestGetTextEmptyHTML()
{
    Element header;

    std::string text = "";
    header.GetText(text);

    CPPUNIT_ASSERT(text.empty());
}

void ElementTest::TestSetText()
{
    Element headerText;
    headerText.SetValue("header");

    Element header("h1");
    header.AddToEnd(headerText);

    Element div("div");
    div.AddToEnd(header);

    div.SetText("Test");

    auto& children = div.GetChildren();

    CPPUNIT_ASSERT(children.at(0).GetName().empty());
    CPPUNIT_ASSERT(children.at(0).GetValue() == "Test");
}

void ElementTest::TestSetTextEmptyString()
{
    Element headerText;
    headerText.SetValue("header");

    Element header("h1");
    header.AddToEnd(headerText);

    Element div("div");
    div.AddToEnd(header);

    div.SetText("");

    CPPUNIT_ASSERT(div.GetChildren().empty());
}