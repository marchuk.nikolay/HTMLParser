#pragma once

#include "cppunit\TestFixture.h"
#include "cppunit\extensions\HelperMacros.h"

class ElementParserTest : public CppUnit::TestFixture
{
public:
    void TestParseValueElement();
    void TestParseSingleTagElement();
    void TestParseDefaultElement();

    CPPUNIT_TEST_SUITE(ElementParserTest);

    CPPUNIT_TEST(TestParseValueElement);
    CPPUNIT_TEST(TestParseSingleTagElement);
    CPPUNIT_TEST(TestParseDefaultElement);

    CPPUNIT_TEST_SUITE_END();
};