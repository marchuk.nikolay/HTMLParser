#include "stdafx.h"
#include "TagCreator.h"
#include "TagCreatorTest.h"

using namespace HTMLParserLibrary;

void TagCreatorTest::TestCreateOpenTag()
{
    Element element("div");

    TagCreator tagCreator;
    std::string tag = "";

    tagCreator.CreateOpenTag(element, tag);

    CPPUNIT_ASSERT(tag == "<div>");
}

void TagCreatorTest::TestCreateOpenTagWithAttributes()
{
    Attribute disabled("disabled");
    Attribute classAttribute("class", "headers");

    Element element("h1");
    element.AddAttribute(disabled);
    element.AddAttribute(classAttribute);

    TagCreator tagCreator;
    std::string tag = "";

    tagCreator.CreateOpenTag(element, tag);

    CPPUNIT_ASSERT(tag == "<h1 disabled class=\"headers\">");
}

void TagCreatorTest::TestCreateCloseTag()
{
    Element element("div");

    TagCreator tagCreator;
    std::string tag = "";

    tagCreator.CreateCloseTag(element, tag);

    CPPUNIT_ASSERT(tag == "</div>");
}