#include "stdafx.h"
#include "AttributeTest.h"
#include "Attribute.h"

using namespace HTMLParserLibrary;

void AttributeTest::TestGetName()
{
    Attribute attribute("title", "Paragraph");
    CPPUNIT_ASSERT(attribute.GetName() == "title");
}

void AttributeTest::TestGetValue()
{
    Attribute attribute("title", "Paragraph");
    CPPUNIT_ASSERT(attribute.GetValue() == "Paragraph");
}

void AttributeTest::TestSetName()
{
    Attribute attribute("id", "p1");
    attribute.SetName("class");

    CPPUNIT_ASSERT(attribute.GetName() == "class");
}

void AttributeTest::TestSetValue()
{
    Attribute attribute("id", "p1");
    attribute.SetValue("p2");

    CPPUNIT_ASSERT(attribute.GetValue() == "p2");
}

void AttributeTest::TestSetItem()
{
    Attribute attribute("id", "p1");
    attribute.SetItem("class", "p2");

    CPPUNIT_ASSERT(attribute.GetName() == "class");
    CPPUNIT_ASSERT(attribute.GetValue() == "p2");
}

void AttributeTest::TestAttributeWithoutValue()
{
    Attribute attribute("disabled");
    CPPUNIT_ASSERT(attribute.GetName() == "disabled");
    CPPUNIT_ASSERT(attribute.GetValue().empty());
}