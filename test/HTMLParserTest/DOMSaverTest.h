#pragma once

#include "cppunit\TestFixture.h"
#include "cppunit\extensions\HelperMacros.h"

class DOMSaverTest : public CppUnit::TestFixture
{
public:
    void TestSaveSingleElement();
    void TestSaveDOMWithoutNestingAndAttributes();
    void TestSaveElementWithAttributes();
    void TestSaveElementWithNesting();
    void TestSaveElementWithValueAndAttributes();
    void TestSaveElementWithFewValues();
    void TestSaveElementWithNestingAndAttributes();
    void TestSaveSingleTagElements();

    CPPUNIT_TEST_SUITE(DOMSaverTest);

    CPPUNIT_TEST(TestSaveSingleElement);
    CPPUNIT_TEST(TestSaveDOMWithoutNestingAndAttributes);
    CPPUNIT_TEST(TestSaveElementWithAttributes);
    CPPUNIT_TEST(TestSaveElementWithNesting);
    CPPUNIT_TEST(TestSaveElementWithValueAndAttributes);
    CPPUNIT_TEST(TestSaveElementWithFewValues);
    CPPUNIT_TEST(TestSaveElementWithNestingAndAttributes);
    CPPUNIT_TEST(TestSaveSingleTagElements);

    CPPUNIT_TEST_SUITE_END();
};